﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PublicacionesConjuntosByIdLista
    {
        public virtual ICollection<SP_LIST_PUBLICACIONESCONJUNTOSBYID_Result> Lista { get; set; }

        public PublicacionesConjuntosByIdLista()
        {
            Lista = new HashSet<SP_LIST_PUBLICACIONESCONJUNTOSBYID_Result>();
        }
    }
}