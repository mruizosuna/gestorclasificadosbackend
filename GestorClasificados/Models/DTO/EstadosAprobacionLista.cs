﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class EstadosAprobacionLista
    {
        public virtual ICollection<SP_LIST_ESTADOSAPROBACION_Result> Lista { get; set; }

        public EstadosAprobacionLista()
        {
            Lista = new HashSet<SP_LIST_ESTADOSAPROBACION_Result>();
        }

    }
}