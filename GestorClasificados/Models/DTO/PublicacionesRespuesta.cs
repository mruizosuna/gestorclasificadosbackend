﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PublicacionesRespuesta : RespuestaBase
    {
        public int PublicacionID { get; set; }
        public int UsuarioID { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public int TipoPublicacionID { get; set; }
        public int AlcanceID { get; set; }
        public int PeriodoID { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public int EstadoAprobacionID { get; set; }
        public bool PagoRealizado { get; set; }

    }

}