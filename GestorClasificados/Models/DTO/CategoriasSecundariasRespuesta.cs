﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class CategoriasSecundariasRespuesta : RespuestaBase
    {
        public int SubcategoriaID { get; set; }
        public int CategoriaID { get; set; }
        public string NombreSubcategoria { get; set; }
    }
}