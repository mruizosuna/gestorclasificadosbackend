﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PublicacionSubCategoriasRespuesta : RespuestaBase
    {
        public int PublicacionSubCategoriaID { get; set; }
        public int PublicacionID { get; set; }
        public int SubCategoriaID { get; set; }
    }
}