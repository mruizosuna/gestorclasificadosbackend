﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class CategoriasPrincipalesLista
    {
        public virtual ICollection<SP_LIST_CATEGORIASPRINCIPALES_Result> Lista { get; set; }
        public CategoriasPrincipalesLista()
        {
            Lista = new HashSet<SP_LIST_CATEGORIASPRINCIPALES_Result>();
        }
    }
}