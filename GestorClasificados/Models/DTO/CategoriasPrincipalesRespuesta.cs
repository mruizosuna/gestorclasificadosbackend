﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class CategoriasPrincipalesRespuesta : RespuestaBase
    {
        public int CategoriaID { get; set; }
        public string NombreCategoria { get; set; }
    }
}