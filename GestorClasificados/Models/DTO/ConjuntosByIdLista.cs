﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class ConjuntosByIdLista
    {
        public virtual ICollection<SP_LIST_CONJUNTOSBYID_Result> Lista { get; set; }

        public ConjuntosByIdLista()
        {
            Lista = new HashSet<SP_LIST_CONJUNTOSBYID_Result>();
        }
    }
}