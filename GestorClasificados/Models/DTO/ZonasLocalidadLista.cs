﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class ZonasLocalidadLista
    {
        public virtual ICollection<SP_LIST_ZONASLOCALIDAD_Result> Lista { get; set; }

        public ZonasLocalidadLista()
        {
            Lista = new HashSet<SP_LIST_ZONASLOCALIDAD_Result>();
        }
    }
}