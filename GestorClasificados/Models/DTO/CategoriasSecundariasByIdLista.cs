﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class CategoriasSecundariasByIdLista
    {
        public virtual ICollection<SP_LIST_CATEGORIASSECUNDARIASBYID_Result> Lista { get; set; }

        public CategoriasSecundariasByIdLista()
        {
            Lista = new HashSet<SP_LIST_CATEGORIASSECUNDARIASBYID_Result>();
        }

    }
}