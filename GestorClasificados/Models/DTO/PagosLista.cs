﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PagosLista
    {
        public virtual ICollection<SP_LIST_PAGOS_Result> Lista { get; set; }

        public PagosLista()
        {
            Lista = new HashSet<SP_LIST_PAGOS_Result>();
        }
    }
}