﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class ZonasLocalidadRespuesta : RespuestaBase
    {
        public int ZonasLocalidadID { get; set; }
        public string NombreZonaLocalidad { get; set; }
    }
}