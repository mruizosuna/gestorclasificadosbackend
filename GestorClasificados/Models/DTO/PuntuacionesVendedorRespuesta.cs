﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PuntuacionesVendedorRespuesta : RespuestaBase
    {
        public int PuntuacionID { get; set; }
        public int VendedorID { get; set; }
        public int Puntuacion { get; set; }
        public string Comentario { get; set; }

    }
}