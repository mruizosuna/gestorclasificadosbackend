﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class ConjuntosLista
    {
        public virtual ICollection<SP_LIST_CONJUNTOS_Result> Lista { get; set; }

        public ConjuntosLista()
        {
            Lista = new HashSet<SP_LIST_CONJUNTOS_Result>();
        }
    }
}