﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PuntuacionesClasificadoLista
    {
        public virtual ICollection<SP_LIST_PUNTUACIONESCLASIFICADO_Result> Lista { get; set; }

        public PuntuacionesClasificadoLista()
        {
            Lista = new HashSet<SP_LIST_PUNTUACIONESCLASIFICADO_Result>();
        }

    }
}