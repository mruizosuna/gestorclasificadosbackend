﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class EstadosAprobacionRespuesta : RespuestaBase
    {
        public int EstadoID { get; set; }
        public string NombreEstado { get; set; }
    }
}