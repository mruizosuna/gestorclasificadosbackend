﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PublicacionSubCategoriasLista
    {
        public virtual ICollection<SP_LIST_PUBLICACIONSUBCATEGORIAS_Result> Lista { get; set; }

        public PublicacionSubCategoriasLista()
        {
            Lista = new HashSet<SP_LIST_PUBLICACIONSUBCATEGORIAS_Result>();
        }
    }
}