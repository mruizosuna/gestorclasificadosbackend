﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class ConjuntosRespuesta : RespuestaBase
    {
        public int ConjuntoID { get; set; }

        public string NombreConjunto { get; set; }
        public int ZonaLocalidadID { get; set; }
    }
}