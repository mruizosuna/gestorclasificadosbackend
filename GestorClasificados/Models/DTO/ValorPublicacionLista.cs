﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class ValorPublicacionLista
    {
        public virtual ICollection<SP_LIST_VALORPUBLICACION_Result> Lista { get; set; }

        public ValorPublicacionLista()
        {
            Lista = new HashSet<SP_LIST_VALORPUBLICACION_Result>();
        }
    }
}