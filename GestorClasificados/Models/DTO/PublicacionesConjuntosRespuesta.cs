﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PublicacionesConjuntosRespuesta : RespuestaBase
    {
        public int PublicacionConjuntoID { get; set; }
        public int PublicacionID { get; set; }
        public int ConjuntoID { get; set; }
        public int? ZonasLocalidadID { get; set; }
    }
}