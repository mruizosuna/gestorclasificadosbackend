﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class VideosPublicacionLista
    {
        public virtual ICollection<SP_LIST_VIDEOSPUBLICACION_Result> Lista { get; set; }

        public VideosPublicacionLista()
        {
            Lista = new HashSet<SP_LIST_VIDEOSPUBLICACION_Result>();
        }
    }
}