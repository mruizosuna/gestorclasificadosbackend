﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class AlcanceRespuesta : RespuestaBase
    {
        public int AlcanceID { get; set; }
        public string NombreAlcance { get; set; }
        public int? CantidadAlcance { get; set; }
        public string TipoAlcance { get; set; }
        public int? TipoPublicacionID { get; set; }

    }
}