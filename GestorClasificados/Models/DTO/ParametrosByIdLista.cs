﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class ParametrosByIdLista
    {
        public virtual ICollection<SP_LIST_PARAMETROSBYID_Result> Lista { get; set; }

        public ParametrosByIdLista()
        {
            Lista = new HashSet<SP_LIST_PARAMETROSBYID_Result>();
        }
    }
}