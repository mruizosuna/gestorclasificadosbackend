﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PagosRespuesta : RespuestaBase
    {
        public int PagoID { get; set; }
        public int PublicacionID { get; set; }
        public DateTime FechaPago { get; set; }
        public decimal MontoaPagar { get; set; }
        public decimal Descuentos { get; set; }
        public string VoucherDescuento { get; set; }
        public decimal MontoPagado { get; set; }
        public string NumeroReferencia { get; set; }
        public int? TipoPago { get; set; }
        public string RazonSocial { get; set; }
        public int? TipoDocumentoID { get; set; }
        public string NumeroDocumento { get; set; }
        public string TelefonoCelular { get; set; }
        public string Email { get; set; }
        public int? Estado { get; set; }
        public string DescuentoCodeID { get; set; }
        public string Auditoria { get; set; }


    }
}