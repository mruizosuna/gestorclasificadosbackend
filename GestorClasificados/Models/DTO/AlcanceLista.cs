﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GestorClasificados.Models.EDMX;

namespace GestorClasificados.Models.DTO
{
    public class AlcanceLista
    {
        public virtual ICollection<SP_LIST_ALCANCES_Result> Lista { get; set; }

        public AlcanceLista()
        {
            Lista = new HashSet<SP_LIST_ALCANCES_Result>();
        }


    }

}