﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PuntuacionesVendedorLista
    {
        public virtual ICollection<SP_LIST_PUNTUACIONESVENDEDOR_Result> Lista { get; set; }

        public PuntuacionesVendedorLista()
        {
            Lista = new HashSet<SP_LIST_PUNTUACIONESVENDEDOR_Result>();
        }
    }
}
