﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PuntuacionesClasificadoRespuesta : RespuestaBase
    {
        public int PuntuacionID { get; set; }
        public int PublicacionID { get; set; }
        public int Puntuacion { get; set; }
        public string Comentario { get; set; }

    }
}