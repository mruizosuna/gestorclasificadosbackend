﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class VideosPublicacionRespuesta : RespuestaBase
    {
        public int VideoID { get; set; }
        public int PublicacionID { get; set; }
        public string RutaVideo { get; set; }
    }
}