﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PerfilUsuarioLista
    {
        public virtual ICollection<SP_LIST_PERFILUSUARIO_Result> Lista { get; set; }

        public PerfilUsuarioLista()
        {
            Lista = new HashSet<SP_LIST_PERFILUSUARIO_Result>();
        }
    }
}