﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GestorClasificados.Models.EDMX;

namespace GestorClasificados.Models.DTO
{
    public class PublicacionesBuscadorLista
    {
        public virtual ICollection<SP_LIST_PUBLICACIONES_BUSQUEDA_Result> Lista { get; set; }

        public PublicacionesBuscadorLista()
        {
            Lista = new HashSet<SP_LIST_PUBLICACIONES_BUSQUEDA_Result>();
        }


    }

}