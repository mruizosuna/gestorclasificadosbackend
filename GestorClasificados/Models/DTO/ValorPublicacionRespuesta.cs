﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class ValorPublicacionRespuesta : RespuestaBase
    {
        public int ValorPublicacionID { get; set; }
        public int TipoPublicacionID { get; set; }
        public decimal ValorBasePublicacion { get; set; } 
    }
}