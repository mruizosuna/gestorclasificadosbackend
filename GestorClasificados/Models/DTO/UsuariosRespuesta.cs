﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class UsuariosRespuesta : RespuestaBase
    {
        public int UserID { get; set; }
        public string Nombre { get; set; }
        public string CorreoElectronico { get; set; }
        public string Contrasena { get; set; }
        public int PerfilUsuarioID { get; set; }
        public string DescripcionPerfil { get; set; }

    }
}