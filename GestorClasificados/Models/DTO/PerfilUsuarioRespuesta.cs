﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PerfilUsuarioRespuesta : RespuestaBase
    {
        public int PerfilID { get; set; }
        public string DescripcionPerfil { get; set; } 
    }
}