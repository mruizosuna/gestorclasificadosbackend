﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class VideosPublicacionByIdLista
    {
        public virtual ICollection<SP_LIST_VIDEOSPUBLICACIONBYID_Result> Lista { get; set; }

        public VideosPublicacionByIdLista()
        {
            Lista = new HashSet<SP_LIST_VIDEOSPUBLICACIONBYID_Result>();
        }
    }
}