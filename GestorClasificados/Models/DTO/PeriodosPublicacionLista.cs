﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PeriodosPublicacionLista
    {
        public virtual ICollection<SP_LIST_PERIODOSPUBLICACION_Result> Lista { get; set; }

        public PeriodosPublicacionLista()
        {
            Lista = new HashSet<SP_LIST_PERIODOSPUBLICACION_Result>();
        }
    }
}