﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PublicacionSubCategoriasListaById
    {
        public virtual ICollection<SP_LIST_PUBLICACIONSUBCATEGORIASBYID_Result> Lista { get; set; }

        public PublicacionSubCategoriasListaById()
        {
            Lista = new HashSet<SP_LIST_PUBLICACIONSUBCATEGORIASBYID_Result>();
        }
    }
}