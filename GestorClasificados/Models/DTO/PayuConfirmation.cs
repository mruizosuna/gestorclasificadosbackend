﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PayUConfirmation
    {
        public string ResponseCodePol { get; set; }
        public string Phone { get; set; }
        public decimal AdditionalValue { get; set; }
        public bool Test { get; set; }
        public DateTime TransactionDate { get; set; }
        public string CcNumber { get; set; }
        public string CcHolder { get; set; }
        public string ErrorCodeBank { get; set; }
        public string BillingCountry { get; set; }
        public string BankReferencedName { get; set; }
        public string Description { get; set; }
        public decimal AdministrativeFeeTax { get; set; }
        public decimal Value { get; set; }
        public decimal AdministrativeFee { get; set; }
        public int PaymentMethodType { get; set; }
        public string OfficePhone { get; set; }
        public string EmailBuyer { get; set; }
        public string ResponseMessagePol { get; set; }
        public string ErrorMessageBank { get; set; }
        public string ShippingCity { get; set; }
        public string TransactionId { get; set; }
        public string Sign { get; set; }
        public decimal Tax { get; set; }
        public int PaymentMethod { get; set; }
        public string BillingAddress { get; set; }
        public string PaymentMethodName { get; set; }
        public string PseBank { get; set; }
        public int StatePol { get; set; }
        public DateTime Date { get; set; }
        public string NicknameBuyer { get; set; }
        public string ReferencePol { get; set; }
        public string Currency { get; set; }
        public decimal Risk { get; set; }
        public string ShippingAddress { get; set; }
        public int BankId { get; set; }
        public string PaymentRequestState { get; set; }
        public int CustomerNumber { get; set; }
        public decimal AdministrativeFeeBase { get; set; }
        public int Attempts { get; set; }
        public int MerchantId { get; set; }
        public decimal ExchangeRate { get; set; }
        public string ShippingCountry { get; set; }
        public int InstallmentsNumber { get; set; }
        public string Franchise { get; set; }
        public int PaymentMethodId { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public string AntifraudMerchantId { get; set; }
        public string Extra3 { get; set; }
        public string NicknameSeller { get; set; }
        public string Ip { get; set; }
        public string AirlineCode { get; set; }
        public string BillingCity { get; set; }
        public string PseReference1 { get; set; }
        public string ReferenceSale { get; set; }
        public string PseReference3 { get; set; }
        public string PseReference2 { get; set; }
    }

}