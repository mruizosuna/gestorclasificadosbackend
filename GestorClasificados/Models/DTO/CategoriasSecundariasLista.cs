﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class CategoriasSecundariasLista
    {
        public virtual ICollection<SP_LIST_CATEGORIASSECUNDARIAS_Result> Lista { get; set; }

        public CategoriasSecundariasLista()
        {
            Lista = new HashSet<SP_LIST_CATEGORIASSECUNDARIAS_Result>();
        }

    }
}