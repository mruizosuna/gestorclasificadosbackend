﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PublicacionesListaUsuario
    {
        public virtual ICollection<SP_LIST_PUBLICACIONES_USUARIO_Result> Lista { get; set; }

        public PublicacionesListaUsuario()
        {
            Lista = new HashSet<SP_LIST_PUBLICACIONES_USUARIO_Result>();
        }
    }

}