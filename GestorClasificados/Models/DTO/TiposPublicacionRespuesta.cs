﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class TiposPublicacionRespuesta : RespuestaBase
    {
        public int TipoPublicacionID { get; set; }
        public string NombreTipoPublicacion { get; set; }
        public string DescripcionTipoPublicacion { get; set; }
        public bool? Texto {  get; set; }
        public int? Imagenes { get; set; }
        public int? Video {  get; set; }

    }
}