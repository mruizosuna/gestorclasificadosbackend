﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class DescuentoRespuesta : RespuestaBase
    {
        public string DescuentoCodeID { get; set; }
        public DateTime FechaGeneracion { get; set; }    
        public DateTime FechaVencimiento { get; set; }  
        public bool? Utilizado {  get; set; }    
        public int? UserID { get; set; }



    }
}