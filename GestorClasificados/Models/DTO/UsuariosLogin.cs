﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class UsuariosLogin
    {
        public string CorreoElectronico { get; set; }
        public string Contrasena { get; set; }

    }
}