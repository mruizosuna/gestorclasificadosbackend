﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class UsuariosLista
    {
        public virtual ICollection<SP_LIST_USUARIOS_Result> Lista { get; set; }

        public UsuariosLista()
        {
            Lista = new HashSet<SP_LIST_USUARIOS_Result>();
        }
    }
}