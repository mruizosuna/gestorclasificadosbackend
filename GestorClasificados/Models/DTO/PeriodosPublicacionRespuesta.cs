﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PeriodosPublicacionRespuesta : RespuestaBase
    {
        public int PeriodoID { get; set; }
        public string NombrePeriodo { get; set; }
        public int Dias { get; set; }
        public int PorcentajeValorPeriodo { get; set; }
        public int PorcentajeValorAlcance { get; set; }
        public int TipoPublicacionID { get; set; }


    }
}