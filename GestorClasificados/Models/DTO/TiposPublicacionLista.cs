﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class TiposPublicacionLista
    {
        public virtual ICollection<SP_LIST_TIPOSPUBLICACION_Result> Lista { get; set; }

        public TiposPublicacionLista()
        {
            Lista = new HashSet<SP_LIST_TIPOSPUBLICACION_Result>();
        }
    }
}