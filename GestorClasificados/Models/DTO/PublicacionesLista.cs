﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class PublicacionesLista
    {
        public virtual ICollection<SP_LIST_PUBLICACIONES_Result> Lista { get; set; }

        public PublicacionesLista()
        {
            Lista = new HashSet<SP_LIST_PUBLICACIONES_Result>();
        }
    }

}