﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class ImagenesPublicacionRespuesta : RespuestaBase
    {
        public int ImagenID { get; set; }
        public int PublicacionID { get; set; }
        public string RutaImagen { get; set; }
        public string Imagen64 { get; set; }

    }
}