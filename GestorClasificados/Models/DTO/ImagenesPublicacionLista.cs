﻿using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Models.DTO
{
    public class ImagenesPublicacionLista
    {
        public virtual ICollection<SP_LIST_IMAGENESPUBLICACION_Result> Lista { get; set; }

        public ImagenesPublicacionLista()
        {
            Lista = new HashSet<SP_LIST_IMAGENESPUBLICACION_Result>();
        }
    }
}