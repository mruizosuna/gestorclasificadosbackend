//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestorClasificados.Models.EDMX
{
    using System;
    
    public partial class SP_GET_PAGOS_BYID_Result
    {
        public int PagoID { get; set; }
        public Nullable<int> PublicacionID { get; set; }
        public Nullable<System.DateTime> FechaPago { get; set; }
        public Nullable<decimal> MontoaPagar { get; set; }
        public Nullable<decimal> Descuentos { get; set; }
        public Nullable<decimal> MontoPagado { get; set; }
        public string NumeroReferencia { get; set; }
        public Nullable<int> TipoPago { get; set; }
        public string RazonSocial { get; set; }
        public Nullable<int> TipoDocumentoID { get; set; }
        public string NumeroDocumento { get; set; }
        public string TelefonoCelular { get; set; }
        public string Email { get; set; }
        public Nullable<int> Estado { get; set; }
        public string DescuentoCodeID { get; set; }
        public string Auditoria { get; set; }
    }
}
