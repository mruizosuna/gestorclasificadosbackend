//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GestorClasificados.Models.EDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class CategoriasSecundarias
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CategoriasSecundarias()
        {
            this.PublicacionSubCategorias = new HashSet<PublicacionSubCategorias>();
        }
    
        public int SubcategoriaID { get; set; }
        public Nullable<int> CategoriaID { get; set; }
        public string NombreSubcategoria { get; set; }
    
        public virtual CategoriasPrincipales CategoriasPrincipales { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PublicacionSubCategorias> PublicacionSubCategorias { get; set; }
    }
}
