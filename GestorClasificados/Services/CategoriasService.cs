﻿using GestorClasificados.Models.DTO;
using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Services
{
    public class CategoriasService
    {
        GestorClasificadosEntities db = new GestorClasificadosEntities();


        #region ListaCategoriasPrincipales

        public CategoriasPrincipalesLista GetListaCategoriasPrincipales()
        {
            CategoriasPrincipalesLista respuesta = new CategoriasPrincipalesLista();
            var lista = db.SP_LIST_CATEGORIASPRINCIPALES().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public CategoriasPrincipalesRespuesta AddCategoriasPrincipales(CategoriasPrincipalesRespuesta categoriasPrincipales)
        {

            CategoriasPrincipalesRespuesta respuesta = new CategoriasPrincipalesRespuesta();
            try
            {
                var accion = db.SP_ADD_CATEGORIASPRINCIPALES(
                categoriasPrincipales.NombreCategoria
                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar CategoriasPrincipales -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public CategoriasPrincipalesRespuesta UpdCategoriasPrincipales(CategoriasPrincipalesRespuesta categoriasPrincipalesRespuesta)
        {

            CategoriasPrincipalesRespuesta respuesta = new CategoriasPrincipalesRespuesta();
            try
            {
                var accion = db.SP_UPD_CATEGORIASPRINCIPALES(
                categoriasPrincipalesRespuesta.CategoriaID,
                categoriasPrincipalesRespuesta.NombreCategoria
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar CategoriasPrincipales -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public CategoriasPrincipalesRespuesta GetCategoriasPrincipalesById(int id)
        {
            CategoriasPrincipalesRespuesta respuesta = new CategoriasPrincipalesRespuesta();
            var info = db.SP_GET_CATEGORIASPRINCIPALES_BYID(id).FirstOrDefault();

            respuesta.CategoriaID = info.CategoriaID;
            respuesta.NombreCategoria = info.NombreCategoria;

            return respuesta;
        }


        public CategoriasPrincipalesRespuesta DelCategoriasPrincipales(int id)
        {

            CategoriasPrincipalesRespuesta respuesta = new CategoriasPrincipalesRespuesta();
            try
            {
                var accion = db.SP_DEL_CATEGORIASPRINCIPALES(
                id


             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar CategoriasPrincipales -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }


        #endregion

        #region ListaCategoriasSecundarias

        public CategoriasSecundariasLista GetListaCategoriasSecundarias()
        {
            CategoriasSecundariasLista respuesta = new CategoriasSecundariasLista();
            var lista = db.SP_LIST_CATEGORIASSECUNDARIAS().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }

        public CategoriasSecundariasByIdLista GetListaCategoriasSecundariasById(int id)
        {
            CategoriasSecundariasByIdLista respuesta = new CategoriasSecundariasByIdLista();
            var lista = db.SP_LIST_CATEGORIASSECUNDARIASBYID(id).ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public CategoriasSecundariasRespuesta AddCategoriasSecundarias(CategoriasSecundariasRespuesta categoriasSecundarias)
        {

            CategoriasSecundariasRespuesta respuesta = new CategoriasSecundariasRespuesta();
            try
            {
                var accion = db.SP_ADD_CATEGORIASSECUNDARIAS(
                categoriasSecundarias.CategoriaID,
                categoriasSecundarias.NombreSubcategoria
                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar CategoriasSecundarias -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public CategoriasSecundariasRespuesta UpdCategoriasSecundarias(CategoriasSecundariasRespuesta categoriasSecundarias)
        {

            CategoriasSecundariasRespuesta respuesta = new CategoriasSecundariasRespuesta();
            try
            {
                var accion = db.SP_UPD_CATEGORIASSECUNDARIAS(
                categoriasSecundarias.SubcategoriaID,
                categoriasSecundarias.CategoriaID,
                categoriasSecundarias.NombreSubcategoria
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar CategoriasSecundarias -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public CategoriasSecundariasRespuesta GetCategoriasSecundariasById(int id)
        {
            CategoriasSecundariasRespuesta respuesta = new CategoriasSecundariasRespuesta();
            var info = db.SP_GET_CATEGORIASSECUNDARIAS_BYID(id).FirstOrDefault();

            respuesta.SubcategoriaID = info.SubcategoriaID;
            respuesta.CategoriaID = (int)info.CategoriaID;
            respuesta.NombreSubcategoria = info.NombreSubcategoria;

            return respuesta;
        }


        #endregion
    }
}