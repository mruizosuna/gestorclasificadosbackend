﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using GestorClasificados.Models.DTO;
using GestorClasificados.Models.EDMX;


namespace GestorClasificados.Services
{
    public class ParametricasService
    {
        GestorClasificadosEntities db = new GestorClasificadosEntities();


        #region ListaAlcances

        public AlcanceLista GetListaAlcances()
        {
            AlcanceLista respuesta = new AlcanceLista();
            var lista = db.SP_LIST_ALCANCES().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public AlcanceRespuesta AddAlcance(AlcanceRespuesta alcance)
        {

            AlcanceRespuesta respuesta = new AlcanceRespuesta();
            try
            {
                var accion = db.SP_ADD_ALCANCES(
                alcance.NombreAlcance,
                alcance.CantidadAlcance,
                alcance.TipoAlcance,
                alcance.TipoPublicacionID
                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar Alcance -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public AlcanceRespuesta UpdAlcance(AlcanceRespuesta alcance)
        {

            AlcanceRespuesta respuesta = new AlcanceRespuesta();
            try
            {
                var accion = db.SP_UPD_ALCANCES(
                alcance.AlcanceID,
                alcance.NombreAlcance,
                alcance.CantidadAlcance,
                alcance.TipoAlcance,
                alcance.TipoPublicacionID
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar Alcance -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public AlcanceRespuesta GetAlcanceById(int id)
        {
            AlcanceRespuesta respuesta = new AlcanceRespuesta();
            var info = db.SP_GET_ALCANCES_BYID(id).FirstOrDefault();

            respuesta.AlcanceID = info.AlcanceID;
            respuesta.NombreAlcance = info.NombreAlcance;
            respuesta.CantidadAlcance = info.CantidadAlcance;
            respuesta.TipoPublicacionID = info.TipoPublicacionID;


            return respuesta;
        }

        public AlcanceRespuesta DelAlcance(int id)
        {

            AlcanceRespuesta respuesta = new AlcanceRespuesta();
            try
            {
                var accion = db.SP_DEL_ALCANCES(
                id


             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar Alcance -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        #endregion

        #region ListaConjuntos

        public ConjuntosLista GetListaConjuntos()
        {
            ConjuntosLista respuesta = new ConjuntosLista();
            var lista = db.SP_LIST_CONJUNTOS().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }

        public ConjuntosByIdLista GetListaConjuntosById(int id)
        {
            ConjuntosByIdLista respuesta = new ConjuntosByIdLista();
            var lista = db.SP_LIST_CONJUNTOSBYID(id).ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }

        public ConjuntosRespuesta AddConjuntos(ConjuntosRespuesta conjuntos)
        {

            ConjuntosRespuesta respuesta = new ConjuntosRespuesta();
            try
            {
                var accion = db.SP_ADD_CONJUNTOS(
                conjuntos.NombreConjunto,
                conjuntos.ZonaLocalidadID
                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar Conjuntos -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public ConjuntosRespuesta UpdConjuntos(ConjuntosRespuesta conjuntos)
        {

            ConjuntosRespuesta respuesta = new ConjuntosRespuesta();
            try
            {
                var accion = db.SP_UPD_CONJUNTOS(
                conjuntos.ConjuntoID,
                conjuntos.NombreConjunto,
                conjuntos.ZonaLocalidadID
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar Conjuntos -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public ConjuntosRespuesta GetConjuntosById(int id)
        {
            ConjuntosRespuesta respuesta = new ConjuntosRespuesta();
            var info = db.SP_GET_CONJUNTOS_BYID(id).FirstOrDefault();

            respuesta.ConjuntoID =info.ConjuntoID;
            respuesta.NombreConjunto = info.NombreConjunto;
            respuesta.ZonaLocalidadID = (int)info.ZonaLocalidadID;


            return respuesta;
        }

        public ConjuntosRespuesta DelConjuntos(int id)
        {

            ConjuntosRespuesta respuesta = new ConjuntosRespuesta();
            try
            {
                var accion = db.SP_DEL_CONJUNTOS(
                id


             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar Conjuntos -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        #endregion

        #region ListaEstadosAprobacion

        public EstadosAprobacionLista GetListaEstadosAprobacion()
        {
            EstadosAprobacionLista respuesta = new EstadosAprobacionLista();
            var lista = db.SP_LIST_ESTADOSAPROBACION().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public EstadosAprobacionRespuesta AddEstadosAprobacion(EstadosAprobacionRespuesta estadosAprobacion)
        {

            EstadosAprobacionRespuesta respuesta = new EstadosAprobacionRespuesta();
            try
            {
                var accion = db.SP_ADD_ESTADOSAPROBACION(
                estadosAprobacion.NombreEstado
                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar EstadosAprobacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public EstadosAprobacionRespuesta UpdEstadosAprobacion(EstadosAprobacionRespuesta estadosAprobacion)
        {

            EstadosAprobacionRespuesta respuesta = new EstadosAprobacionRespuesta();
            try
            {
                var accion = db.SP_UPD_ESTADOSAPROBACION(
                estadosAprobacion.EstadoID,
                estadosAprobacion.NombreEstado
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar EstadosAprobacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public EstadosAprobacionRespuesta GetEstadosAprobacionById(int id)
        {
            EstadosAprobacionRespuesta respuesta = new EstadosAprobacionRespuesta();
            var info = db.SP_GET_ESTADOSAPROBACION_BYID(id).FirstOrDefault();

            respuesta.EstadoID = info.EstadoID;
            respuesta.NombreEstado = info.NombreEstado;


            return respuesta;
        }

        #endregion

        #region ListaPagos

        public PagosLista GetListaPagos()
        {
            PagosLista respuesta = new PagosLista();
            var lista = db.SP_LIST_PAGOS().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public PagosRespuesta AddPagos(PagosRespuesta pagos)
        {

            PagosRespuesta respuesta = new PagosRespuesta();
            try
            {
                var accion = db.SP_ADD_PAGOS(
            
                pagos.PublicacionID,
                pagos.FechaPago,
                pagos.MontoaPagar,
                pagos.Descuentos,
                pagos.MontoPagado,
                pagos.NumeroReferencia,
                pagos.TipoPago,
                pagos.RazonSocial,
                pagos.TipoDocumentoID,
                pagos.NumeroDocumento,
                pagos.TelefonoCelular,
                pagos.Email,
                pagos.Estado,
                pagos.DescuentoCodeID,
                pagos.Auditoria
                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar EstadosAprobacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PagosRespuesta UpdPagos(PagosRespuesta pagos)
        {

            PagosRespuesta respuesta = new PagosRespuesta();
            try
            {
                var accion = db.SP_UPD_PAGOS(
                pagos.PagoID,
                pagos.PublicacionID,
                pagos.FechaPago,
                pagos.MontoaPagar,
                pagos.Descuentos,
                pagos.MontoPagado,
                pagos.NumeroReferencia,
                pagos.TipoPago,
                pagos.RazonSocial,
                pagos.TipoDocumentoID,
                pagos.NumeroDocumento,
                pagos.TelefonoCelular,
                pagos.Email,
                pagos.Estado,
                pagos.DescuentoCodeID,
                pagos.Auditoria

             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar EstadosAprobacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PagosRespuesta GetPagosById(int id)
        {
            PagosRespuesta respuesta = new PagosRespuesta();
            var info = db.SP_GET_PAGOS_BYID(id).FirstOrDefault();

            respuesta.PagoID = info.PagoID;
            respuesta.PublicacionID = (int)info.PublicacionID;
            respuesta.FechaPago = (DateTime)info.FechaPago;
            respuesta.MontoaPagar = (decimal)info.MontoaPagar;
            respuesta.Descuentos = (decimal)info.Descuentos;
            respuesta.MontoPagado = (decimal)info.MontoPagado;
            respuesta.NumeroReferencia = info.NumeroReferencia;
            respuesta.TipoPago = info.TipoPago;
            respuesta.RazonSocial = info.RazonSocial;
            respuesta.TipoDocumentoID = info.TipoDocumentoID;
            respuesta.NumeroDocumento = info.NumeroDocumento;
            respuesta.TelefonoCelular = info.TelefonoCelular;
            respuesta.Email = info.Email;
            respuesta.Estado = info.Estado;
            respuesta.Auditoria = info.Auditoria;


            return respuesta;
        }

        public DescuentoRespuesta GetDescuentoById(string id, int userid)
        {
            DescuentoRespuesta respuesta = new DescuentoRespuesta();
            var info = db.SP_GET_DESCUENTO_BYID(id, userid).FirstOrDefault();

            respuesta.DescuentoCodeID = info.DescuentoCodeID;
            respuesta.FechaGeneracion = info.FechaGeneracion;
            respuesta.FechaVencimiento = info.FechaVencimiento;
            respuesta.Utilizado = info.Utilizado;
            respuesta.UserID = info.UserID;



            return respuesta;
        }


        public DescuentoRespuesta UpdDescuento(DescuentoRespuesta descuento)
        {

            DescuentoRespuesta respuesta = new DescuentoRespuesta();
            try
            {
                var accion = db.SP_UPD_DESCUENTO(
                descuento.DescuentoCodeID,
                descuento.UserID

             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar EstadosAprobacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }
        #endregion

        #region ListaZonasLocalidad

        public ZonasLocalidadLista GetListaZonasLocalidad()
        {
            ZonasLocalidadLista respuesta = new ZonasLocalidadLista();
            var lista = db.SP_LIST_ZONASLOCALIDAD().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public ZonasLocalidadRespuesta AddZonasLocalidad(ZonasLocalidadRespuesta zonasLocalidad)
        {

            ZonasLocalidadRespuesta respuesta = new ZonasLocalidadRespuesta();
            try
            {
                var accion = db.SP_ADD_ZONASLOCALIDAD(

                zonasLocalidad.NombreZonaLocalidad
                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar ZonasLocalidad -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public ZonasLocalidadRespuesta UpdZonasLocalidad(ZonasLocalidadRespuesta zonasLocalidad)
        {

            ZonasLocalidadRespuesta respuesta = new ZonasLocalidadRespuesta();
            try
            {
                var accion = db.SP_UPD_ZONASLOCALIDAD(
                zonasLocalidad.ZonasLocalidadID,
                zonasLocalidad.NombreZonaLocalidad


             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar ZonasLocalidad -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public ZonasLocalidadRespuesta DelZonasLocalidad(int id)
        {

            ZonasLocalidadRespuesta respuesta = new ZonasLocalidadRespuesta();
            try
            {
                var accion = db.SP_DEL_ZONASLOCALIDAD(
                id


             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar ZonasLocalidad -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }


        public ZonasLocalidadRespuesta GetZonasLocalidadById(int id)
        {
            ZonasLocalidadRespuesta respuesta = new ZonasLocalidadRespuesta();
            var info = db.SP_GET_ZONASLOCALIDAD_BYID(id).FirstOrDefault();

            respuesta.ZonasLocalidadID = info.ZonasLocalidadID;
            respuesta.NombreZonaLocalidad = info.NombreZonaLocalidad;

            return respuesta;
        }

        #endregion

        #region ListaParametros
        public ParametrosByIdLista GetListaParametrosById(string tipo)
        {
            ParametrosByIdLista respuesta = new ParametrosByIdLista();
            var lista = db.SP_LIST_PARAMETROSBYID(tipo).ToList(); 
            respuesta.Lista = lista;
            return respuesta;
        }

        #endregion
    }
}