﻿using GestorClasificados.Models.DTO;
using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GestorClasificados.Services
{
    public class PuntuacionesService
    {
        GestorClasificadosEntities db = new GestorClasificadosEntities();


        #region ListaPuntuacionesClasificado

        public PuntuacionesClasificadoLista GetListaPuntuacionesClasificado()
        {
            PuntuacionesClasificadoLista respuesta = new PuntuacionesClasificadoLista();
            var lista = db.SP_LIST_PUNTUACIONESCLASIFICADO().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public PuntuacionesClasificadoRespuesta AddPuntuacionesClasificado(PuntuacionesClasificadoRespuesta puntuacionesClasificado)
        {

            PuntuacionesClasificadoRespuesta respuesta = new PuntuacionesClasificadoRespuesta();
            try
            {
                var accion = db.SP_ADD_PUNTUACIONESCLASIFICADO(

                puntuacionesClasificado.PublicacionID,
                puntuacionesClasificado.Puntuacion,
                puntuacionesClasificado.Comentario

                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar PuntuacionesClasificado -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PuntuacionesClasificadoRespuesta UpdPuntuacionesClasificado(PuntuacionesClasificadoRespuesta puntuacionesClasificado)
        {

            PuntuacionesClasificadoRespuesta respuesta = new PuntuacionesClasificadoRespuesta();
            try
            {
                var accion = db.SP_UPD_PUNTUACIONESCLASIFICADO(
                puntuacionesClasificado.PuntuacionID,
                puntuacionesClasificado.PublicacionID,
                puntuacionesClasificado.Puntuacion,
                puntuacionesClasificado.Comentario
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar PuntuacionesClasificado -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PuntuacionesClasificadoRespuesta GetPuntuacionesClasificadoById(int id)
        {
            PuntuacionesClasificadoRespuesta respuesta = new PuntuacionesClasificadoRespuesta();
            var info = db.SP_GET_PUNTUACIONESCLASIFICADO_BYID(id).FirstOrDefault();

            respuesta.PuntuacionID = info.PuntuacionID;
            respuesta.PublicacionID = (int)info.PublicacionID;
            respuesta.Puntuacion = (int)info.Puntuacion;
            respuesta.Comentario = info.Comentario;

            return respuesta;
        }


        #endregion

        #region ListaPuntuacionesVendedor

        public PuntuacionesVendedorLista GetListaPuntuacionesVendedor()
        {
            PuntuacionesVendedorLista respuesta = new PuntuacionesVendedorLista();
            var lista = db.SP_LIST_PUNTUACIONESVENDEDOR().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public PuntuacionesVendedorRespuesta AddPuntuacionesVendedor(PuntuacionesVendedorRespuesta puntuacionesVendedor)
        {

            PuntuacionesVendedorRespuesta respuesta = new PuntuacionesVendedorRespuesta();
            try
            {
                var accion = db.SP_ADD_PUNTUACIONESVENDEDOR(

                puntuacionesVendedor.VendedorID,
                puntuacionesVendedor.Puntuacion,
                puntuacionesVendedor.Comentario

                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar PuntuacionesVendedor -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PuntuacionesVendedorRespuesta UpdPuntuacionesVendedor(PuntuacionesVendedorRespuesta PuntuacionesVendedor)
        {

            PuntuacionesVendedorRespuesta respuesta = new PuntuacionesVendedorRespuesta();
            try
            {
                var accion = db.SP_UPD_PUNTUACIONESVENDEDOR(
                PuntuacionesVendedor.PuntuacionID,
                PuntuacionesVendedor.VendedorID,
                PuntuacionesVendedor.Puntuacion,
                PuntuacionesVendedor.Comentario
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar PuntuacionesVendedor -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PuntuacionesVendedorRespuesta GetPuntuacionesVendedorById(int id)
        {
            PuntuacionesVendedorRespuesta respuesta = new PuntuacionesVendedorRespuesta();
            var info = db.SP_GET_PUNTUACIONESVENDEDOR_BYID(id).FirstOrDefault();

            respuesta.PuntuacionID = info.PuntuacionID;
            respuesta.VendedorID = (int)info.VendedorID;
            respuesta.Puntuacion = (int)info.Puntuacion;
            respuesta.Comentario = info.Comentario;

            return respuesta;
        }


        #endregion

    }
}