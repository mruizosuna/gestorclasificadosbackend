﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using GestorClasificados.Models.DTO;
using GestorClasificados.Models.EDMX;
using System.Data.Entity.Core.Objects;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing;

namespace GestorClasificados.Services
{
    public class PublicacionesService
    {
        GestorClasificadosEntities db = new GestorClasificadosEntities();
        //christian acosta 
        

        #region ImagenesPublicacion

        public ImagenesPublicacionLista GetListaImagenesPublicacion(int publicacionId)
        {
            ImagenesPublicacionLista respuesta = new ImagenesPublicacionLista();
            var lista = db.SP_LIST_IMAGENESPUBLICACION(publicacionId).ToList(); // Cambio aquí
            respuesta.Lista = lista;    
            return respuesta;
        }


        public async Task<ImagenesPublicacionRespuesta> AddImagenesPublicacionAsync(ImagenesPublicacionRespuesta imagenesPublicacion)
        {

            //Se carga la Imagen en la carpeta de Imagenes y se envia la ruta a la BD
            string ArchivoCargado = "";
            ArchivoCargado = await CargarImagenPublicacion(imagenesPublicacion.PublicacionID.ToString(), imagenesPublicacion.Imagen64);

            ImagenesPublicacionRespuesta respuesta = new ImagenesPublicacionRespuesta();
            try
            {
                var accion = db.SP_ADD_IMAGENESPUBLICACION(

                imagenesPublicacion.PublicacionID,
                ArchivoCargado
                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar ImagenPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

     

        public async Task<string> CargarImagenPublicacion(string publicacionId, string archivoImagen)
        {
            // Determinar la extensión del archivo a partir del prefijo de la cadena base64
            string extension = "";

            // Decodificar el string base64
            byte[] imageData;
            try
            {
                imageData = Convert.FromBase64String(archivoImagen);
            }
            catch (FormatException)
            {
                return "Error: El string base64 no es válido.";
            }

            // Verificar si es PNG
            if (imageData.Length >= 8 &&
                imageData[0] == 0x89 && imageData[1] == 0x50 && imageData[2] == 0x4E &&
                imageData[3] == 0x47 && imageData[4] == 0x0D && imageData[5] == 0x0A &&
                imageData[6] == 0x1A && imageData[7] == 0x0A)
            {
                extension = ".png";
            }
            // Verificar si es JPG
            else if (imageData.Length >= 3 &&
                     imageData[0] == 0xFF && imageData[1] == 0xD8 && imageData[2] == 0xFF)
            {
                extension = ".jpg";
            }
            else
            {
                return "Error: Formato de imagen desconocido.";
            }

            string NombreArchivo = "ImagenPublicacion_" + publicacionId + "_" + Guid.NewGuid().ToString() + extension; // Cambiado a minúsculas para que coincida con la extensión de archivo convencional
            string folderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Imagenes");
            string urlDelBlob = Path.Combine(folderPath, NombreArchivo);


            try
            {
                // Crear el directorio si no existe
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                using (Stream stream = ConvertFromBase64ToStream(archivoImagen))
                {
                    // Redimensionar la imagen
                    using (Bitmap originalBitmap = new Bitmap(stream))
                    {
                        using (Bitmap resizedBitmap = new Bitmap(600, 400))
                        {
                            using (Graphics graphics = Graphics.FromImage(resizedBitmap))
                            {
                                graphics.CompositingQuality = CompositingQuality.HighQuality;
                                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                graphics.SmoothingMode = SmoothingMode.HighQuality;
                                graphics.DrawImage(originalBitmap, 0, 0, 600, 400);

                                // Guardar la imagen redimensionada
                                using (FileStream fileStream = File.Create(urlDelBlob))
                                {
                                    ImageFormat imageFormat = extension == ".png" ? ImageFormat.Png : ImageFormat.Jpeg;
                                    resizedBitmap.Save(fileStream, imageFormat);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Manejar cualquier error y registrar la excepción
                Console.WriteLine($"Error al guardar la imagen: {ex.Message}");
                // Devolver un mensaje de error
                return $"Error al guardar la imagen: {ex.Message}";
            }

            // Devolver la ruta del archivo guardado
            return NombreArchivo;
        }

        private Stream ConvertFromBase64ToStream(string base64String)
        {
            byte[] imageData = Convert.FromBase64String(base64String);
            return new MemoryStream(imageData);
        }
        public ImagenesPublicacionRespuesta UpdImagenesPublicacion(ImagenesPublicacionRespuesta imagenesPublicacion)
        {

            ImagenesPublicacionRespuesta respuesta = new ImagenesPublicacionRespuesta();
            try
            {
                var accion = db.SP_UPD_IMAGENESPUBLICACION(
                imagenesPublicacion.ImagenID,
                imagenesPublicacion.PublicacionID,
                imagenesPublicacion.RutaImagen
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar ImagenPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public ImagenesPublicacionRespuesta GetImagenesPublicacionById(int id)
        {
            ImagenesPublicacionRespuesta respuesta = new ImagenesPublicacionRespuesta();
            var info = db.SP_GET_IMAGENESPUBLICACION_BYID(id).FirstOrDefault();

            respuesta.ImagenID = info.ImagenID;
            respuesta.PublicacionID = (int)info.PublicacionID;
            respuesta.RutaImagen = info.RutaImagen;

            return respuesta;
        }

       public ImagenesPublicacionRespuesta DelImagenPublicacionById(int id)
        {

            ImagenesPublicacionRespuesta respuesta = new ImagenesPublicacionRespuesta();
            try
            {
                var accion = db.SP_DEL_IMAGENESPUBLICACION(id);
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar ImagenPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        #endregion

        #region VideosPublicacion

        public VideosPublicacionLista GetListaVideosPublicacion()
        {
            VideosPublicacionLista respuesta = new VideosPublicacionLista();
            var lista = db.SP_LIST_VIDEOSPUBLICACION().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }

        public VideosPublicacionByIdLista GetListaVideosPublicacionById(int id)
        {
            VideosPublicacionByIdLista respuesta = new VideosPublicacionByIdLista();
            var lista = db.SP_LIST_VIDEOSPUBLICACIONBYID(id).ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public VideosPublicacionRespuesta AddVideosPublicacion(VideosPublicacionRespuesta videosPublicacion)
        {

            VideosPublicacionRespuesta respuesta = new VideosPublicacionRespuesta();
            try
            {
                var accion = db.SP_ADD_VIDEOSPUBLICACION(

                videosPublicacion.PublicacionID,
                videosPublicacion.RutaVideo

                );

                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar VideosPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public VideosPublicacionRespuesta UpdVideosPublicacion(VideosPublicacionRespuesta videosPublicacion)
        {

            VideosPublicacionRespuesta respuesta = new VideosPublicacionRespuesta();
            try
            {
                var accion = db.SP_UPD_VIDEOSPUBLICACION(
                videosPublicacion.VideoID,
                videosPublicacion.PublicacionID,
                videosPublicacion.RutaVideo
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar VideosPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public VideosPublicacionRespuesta GetVideosPublicacionById(int id)
        {
            VideosPublicacionRespuesta respuesta = new VideosPublicacionRespuesta();
            var info = db.SP_GET_VIDEOSPUBLICACION_BYID(id).FirstOrDefault();

            respuesta.VideoID = (int)info.VideoID;
            respuesta.PublicacionID = (int)info.PublicacionID;
            respuesta.RutaVideo = info.RutaVideo;

            return respuesta;
        }

        public VideosPublicacionRespuesta DelVideoPublicacionById(int id)
        {

            VideosPublicacionRespuesta respuesta = new VideosPublicacionRespuesta();
            try
            {
                var accion = db.SP_DEL_VIDEOSPUBLICACION(id);
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar VideoPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        #endregion

        #region PublicacionesConjuntos

        public PublicacionesConjuntosLista GetListaPublicacionesConjuntos()
        {
            PublicacionesConjuntosLista respuesta = new PublicacionesConjuntosLista();
            var lista = db.SP_LIST_PUBLICACIONESCONJUNTOS().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }

        public PublicacionesConjuntosByIdLista GetListaPublicacionesConjuntosById(int id)
        {
            PublicacionesConjuntosByIdLista respuesta = new PublicacionesConjuntosByIdLista();
            var lista = db.SP_LIST_PUBLICACIONESCONJUNTOSBYID(id).ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }

        public PublicacionesConjuntosRespuesta AddPublicacionesConjuntos(PublicacionesConjuntosRespuesta publicacionConjuntos)
        {

            PublicacionesConjuntosRespuesta respuesta = new PublicacionesConjuntosRespuesta();
            try
            {
                var accion = db.SP_ADD_PUBLICACIONESCONJUNTOS(

                publicacionConjuntos.PublicacionID,
                publicacionConjuntos.ConjuntoID,
                publicacionConjuntos.ZonasLocalidadID
                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar PublicacionConjuntos -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PublicacionesConjuntosRespuesta UpdPublicacionesConjuntos(PublicacionesConjuntosRespuesta publicacionConjuntos)
        {

            PublicacionesConjuntosRespuesta respuesta = new PublicacionesConjuntosRespuesta();
            try
            {
                var accion = db.SP_UPD_PUBLICACIONESCONJUNTOS
                    (
                publicacionConjuntos.PublicacionConjuntoID,
                publicacionConjuntos.PublicacionID,
                publicacionConjuntos.ConjuntoID,
                publicacionConjuntos.ZonasLocalidadID
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar PeriodosPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PublicacionesConjuntosRespuesta GetPublicacionesConjuntosById(int id)
        {
            PublicacionesConjuntosRespuesta respuesta = new PublicacionesConjuntosRespuesta();
            var info = db.SP_GET_PUBLICACIONESCONJUNTOS_BYID(id).FirstOrDefault();

            respuesta.PublicacionConjuntoID = info.PublicacionConjuntoID;
            respuesta.PublicacionID = (int)info.PublicacionID;
            respuesta.ConjuntoID = (int)info.ConjuntoID;

            return respuesta;
        }

        public PublicacionesConjuntosRespuesta DelConjuntoPublicacionById(int id)
        {

            PublicacionesConjuntosRespuesta respuesta = new PublicacionesConjuntosRespuesta();
            try
            {
                var accion = db.SP_DEL_CONJUNTOPUBLICACION(id);
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar ConjuntoPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        #endregion

        #region PublicacionSubCategorias

        public PublicacionSubCategoriasLista GetListaPublicacionSubCategorias()
        {
            PublicacionSubCategoriasLista respuesta = new PublicacionSubCategoriasLista();
            var lista = db.SP_LIST_PUBLICACIONSUBCATEGORIAS().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }

        public PublicacionSubCategoriasListaById GetListaPublicacionSubCategoriasById(int id)
        {
            PublicacionSubCategoriasListaById respuesta = new PublicacionSubCategoriasListaById();
            var lista = db.SP_LIST_PUBLICACIONSUBCATEGORIASBYID(id).ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }

        public PublicacionSubCategoriasRespuesta AddPublicacionSubCategorias(PublicacionSubCategoriasRespuesta publicacionSubCategorias)
        {

            PublicacionSubCategoriasRespuesta respuesta = new PublicacionSubCategoriasRespuesta();
            try
            {
                var accion = db.SP_ADD_PUBLICACIONSUBCATEGORIAS(

                publicacionSubCategorias.PublicacionID,
                publicacionSubCategorias.SubCategoriaID

                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar PublicacionSubCategorias -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PublicacionSubCategoriasRespuesta UpdPublicacionSubCategorias(PublicacionSubCategoriasRespuesta publicacionSubCategoriasRespuesta)
        {

            PublicacionSubCategoriasRespuesta respuesta = new PublicacionSubCategoriasRespuesta();
            try
            {
                var accion = db.SP_UPD_PUBLICACIONSUBCATEGORIAS(
                publicacionSubCategoriasRespuesta.PublicacionSubCategoriaID,
                publicacionSubCategoriasRespuesta.PublicacionID,
                publicacionSubCategoriasRespuesta.SubCategoriaID
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar PublicacionSubCategorias -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PublicacionSubCategoriasRespuesta GetPublicacionSubCategoriasById(int id)
        {
            PublicacionSubCategoriasRespuesta respuesta = new PublicacionSubCategoriasRespuesta();
            var info = db.SP_GET_PUBLICACIONSUBCATEGORIAS_BYID(id).FirstOrDefault();

            respuesta.PublicacionSubCategoriaID = info.PublicacionSubCategoriaID;
            respuesta.PublicacionID = (int)info.PublicacionID;
            respuesta.SubCategoriaID = (int)info.SubCategoriaID;
        
            return respuesta;
        }

        public PublicacionSubCategoriasRespuesta DelCategoriaPublicacionById(int id)
        {

            PublicacionSubCategoriasRespuesta respuesta = new PublicacionSubCategoriasRespuesta();
            try
            {
                var accion = db.SP_DEL_CATEGORIAPUBLICACION(id);
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar VideoPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        #endregion

        #region Publicaciones

        public PublicacionesLista GetListaPublicaciones()
        {
            PublicacionesLista respuesta = new PublicacionesLista();
            var lista = db.SP_LIST_PUBLICACIONES().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }

        public PublicacionesListaUsuario GetListaPublicacionesByUser(int id)
        {
            PublicacionesListaUsuario respuesta = new PublicacionesListaUsuario();
            var lista = db.SP_LIST_PUBLICACIONES_USUARIO(id).ToList();
            respuesta.Lista = lista;
            return respuesta;
        }


        public PublicacionesRespuesta AddPublicaciones(PublicacionesRespuesta publicacionesRespuesta)
        {

            PublicacionesRespuesta respuesta = new PublicacionesRespuesta();
            try
            {
                // Crea un ObjectParameter para el parámetro de salida PublicacionId
                ObjectParameter publicacionIdParam = new ObjectParameter("PublicacionId", typeof(int));

                var accion = db.SP_ADD_PUBLICACIONES(

                publicacionesRespuesta.UsuarioID,
                publicacionesRespuesta.FechaCreacion,
                publicacionesRespuesta.FechaVencimiento,
                publicacionesRespuesta.TipoPublicacionID,
                publicacionesRespuesta.AlcanceID,
                publicacionesRespuesta.PeriodoID,
                publicacionesRespuesta.Titulo,
                publicacionesRespuesta.Descripcion,
                publicacionesRespuesta.EstadoAprobacionID,
                publicacionesRespuesta.PagoRealizado,
                publicacionIdParam

                );
                // Lee el valor de PublicacionId del ObjectParameter
                int publicacionId = Convert.ToInt32(publicacionIdParam.Value);

                // Asigna el PublicacionId a la respuesta
                respuesta.PublicacionID = publicacionId;

                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar Publicaciones -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PublicacionesRespuesta UpdPublicaciones(PublicacionesRespuesta publicacionesRespuesta)
        {

            PublicacionesRespuesta respuesta = new PublicacionesRespuesta();
            try
            {
                var accion = db.SP_UPD_PUBLICACIONES(
                publicacionesRespuesta.PublicacionID,
                publicacionesRespuesta.UsuarioID,
                publicacionesRespuesta.FechaCreacion,
                publicacionesRespuesta.FechaVencimiento,
                publicacionesRespuesta.TipoPublicacionID,
                publicacionesRespuesta.AlcanceID,
                publicacionesRespuesta.Titulo,
                publicacionesRespuesta.Descripcion,
                publicacionesRespuesta.PeriodoID,
                publicacionesRespuesta.EstadoAprobacionID,
                publicacionesRespuesta.PagoRealizado
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar Publicaciones -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PublicacionesRespuesta UpdPublicacionEstado(PublicacionesRespuesta publicacionesRespuesta)
        {

            PublicacionesRespuesta respuesta = new PublicacionesRespuesta();
            try
            {
                var accion = db.SP_UPD_PUBLICACION_ESTADO(
                publicacionesRespuesta.PublicacionID,
                publicacionesRespuesta.EstadoAprobacionID,
                publicacionesRespuesta.PagoRealizado
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar Publicaciones -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PublicacionesRespuesta GetPublicacionesById(int id)
        {
            PublicacionesRespuesta respuesta = new PublicacionesRespuesta();
            var info = db.SP_GET_PUBLICACIONES_BYID(id).FirstOrDefault();

            respuesta.PublicacionID = info.PublicacionID;
            respuesta.UsuarioID = (int)info.UsuarioID;
            respuesta.FechaCreacion = (DateTime)info.FechaCreacion;
            respuesta.FechaVencimiento = (DateTime)info.FechaVencimiento;
            respuesta.TipoPublicacionID = (int)info.TipoPublicacionID;
            respuesta.AlcanceID = (int)info.AlcanceID;
            respuesta.Titulo = info.Titulo;
            respuesta.Descripcion = info.Descripcion;
            respuesta.PeriodoID = (int)info.PeriodoID;
            respuesta.EstadoAprobacionID = (int)info.EstadoAprobacionID;
            respuesta.PagoRealizado = (bool)info.PagoRealizado;

            return respuesta;
        }


        public PublicacionesBuscadorLista GetListaBuscadorPublicaciones(int categoriaid, int subcategoriaid, string zona, string palabrasclave)
        {
            if (zona == "undefined")
                zona = null;

            if (palabrasclave == "undefined")
                palabrasclave = null;

            PublicacionesBuscadorLista respuesta = new PublicacionesBuscadorLista();
            var lista = db.SP_LIST_PUBLICACIONES_BUSQUEDA(categoriaid, subcategoriaid, zona, palabrasclave).ToList();
            respuesta.Lista = lista;
            return respuesta;
        }

        public PublicacionesRespuesta DelPublicacionById(int id)
        {

            PublicacionesRespuesta respuesta = new PublicacionesRespuesta();
            try
            {
                var accion = db.SP_DEL_PUBLICACIONES(id);
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar Publicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }
        #endregion


        /*PARAMETRICAS */

        #region PeriodosPublicacion

        public PeriodosPublicacionLista GetListaPeriodosPublicacion()
        {
            PeriodosPublicacionLista respuesta = new PeriodosPublicacionLista();
            var lista = db.SP_LIST_PERIODOSPUBLICACION().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public PeriodosPublicacionRespuesta AddPeriodosPublicacion(PeriodosPublicacionRespuesta periodosPublicacion)
        {

            PeriodosPublicacionRespuesta respuesta = new PeriodosPublicacionRespuesta();
            try
            {
                var accion = db.SP_ADD_PERIODOSPUBLICACION(

                periodosPublicacion.NombrePeriodo,
                periodosPublicacion.Dias,
                periodosPublicacion.PorcentajeValorPeriodo,
                periodosPublicacion.PorcentajeValorAlcance,
                periodosPublicacion.TipoPublicacionID

                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar PeriodosPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PeriodosPublicacionRespuesta UpdPeriodosPublicacion(PeriodosPublicacionRespuesta periodosPublicacion)
        {

            PeriodosPublicacionRespuesta respuesta = new PeriodosPublicacionRespuesta();
            try
            {
                var accion = db.SP_UPD_PERIODOSPUBLICACION(
                periodosPublicacion.PeriodoID,
                periodosPublicacion.NombrePeriodo,
                periodosPublicacion.PorcentajeValorPeriodo,
                periodosPublicacion.PorcentajeValorAlcance,
                periodosPublicacion.TipoPublicacionID,
                periodosPublicacion.Dias
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar PeriodosPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PeriodosPublicacionRespuesta GetPeriodosPublicacionById(int id)
        {
            PeriodosPublicacionRespuesta respuesta = new PeriodosPublicacionRespuesta();
            var info = db.SP_GET_PERIODOSPUBLICACION_BYID(id).FirstOrDefault();

            respuesta.PeriodoID = info.PeriodoID;
            respuesta.NombrePeriodo = info.NombrePeriodo;
            respuesta.PorcentajeValorPeriodo = (int)info.PorcentajeValorPeriodo;

            return respuesta;
        }

        public PeriodosPublicacionRespuesta DelPeriodosPublicacion(int id)
        {

            PeriodosPublicacionRespuesta respuesta = new PeriodosPublicacionRespuesta();
            try
            {
                var accion = db.SP_DEL_PERIODOSPUBLICACION(
                id


             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar PeriodosPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }
        #endregion

        #region TiposPublicacion

        public TiposPublicacionLista GetListaTiposPublicacion()
        {
            TiposPublicacionLista respuesta = new TiposPublicacionLista();
            var lista = db.SP_LIST_TIPOSPUBLICACION().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public TiposPublicacionRespuesta AddTiposPublicacion(TiposPublicacionRespuesta tiposPublicacion)
        {

            TiposPublicacionRespuesta respuesta = new TiposPublicacionRespuesta();
            try
            {
                var accion = db.SP_ADD_TIPOSPUBLICACION(
                tiposPublicacion.NombreTipoPublicacion,
                tiposPublicacion.DescripcionTipoPublicacion,
                tiposPublicacion.Texto ,
                tiposPublicacion.Imagenes,
                tiposPublicacion.Video
                
                );
                respuesta.OperacionExitosa = true;
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar TiposPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public TiposPublicacionRespuesta UpdTiposPublicacion(TiposPublicacionRespuesta tiposPublicacion)
        {

            TiposPublicacionRespuesta respuesta = new TiposPublicacionRespuesta();
            try
            {
                var accion = db.SP_UPD_TIPOSPUBLICACION(
                tiposPublicacion.TipoPublicacionID,
                tiposPublicacion.NombreTipoPublicacion,
                tiposPublicacion.DescripcionTipoPublicacion,
                tiposPublicacion.Texto ,
                tiposPublicacion.Imagenes,
                tiposPublicacion.Video  
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar TiposPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public TiposPublicacionRespuesta GetTiposPublicacionById(int id)
        {
            TiposPublicacionRespuesta respuesta = new TiposPublicacionRespuesta();
            var info = db.SP_GET_TIPOSPUBLICACION_BYID(id).FirstOrDefault();

            respuesta.TipoPublicacionID = info.TipoPublicacionID;
            respuesta.NombreTipoPublicacion = info.NombreTipoPublicacion;
            respuesta.DescripcionTipoPublicacion = info.DescripcionTipoPublicacion;
            respuesta.Video = info.Video;
            respuesta.Texto = info.Texto;
            respuesta.Imagenes = info.Imagenes;

            return respuesta;
        }

        public TiposPublicacionRespuesta DelTiposPublicacion(int id)
        {

            TiposPublicacionRespuesta respuesta = new TiposPublicacionRespuesta();
            try
            {
                var accion = db.SP_DEL_TIPOSPUBLICACION(
                id


             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar TiposPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }
        #endregion

        #region ValorPublicacionLista

        public ValorPublicacionLista GetListaValorPublicacion()
        {
            ValorPublicacionLista respuesta = new ValorPublicacionLista();
            var lista = db.SP_LIST_VALORPUBLICACION().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public ValorPublicacionRespuesta AddValorPublicacion(ValorPublicacionRespuesta valorPublicacion)
        {

            ValorPublicacionRespuesta respuesta = new ValorPublicacionRespuesta();
            try
            {
                var accion = db.SP_ADD_VALORPUBLICACION(

                valorPublicacion.TipoPublicacionID,
                valorPublicacion.ValorBasePublicacion

                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar ValorPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public ValorPublicacionRespuesta UpdValorPublicacion(ValorPublicacionRespuesta ValorPublicacion)
        {

            ValorPublicacionRespuesta respuesta = new ValorPublicacionRespuesta();
            try
            {
                var accion = db.SP_UPD_VALORPUBLICACION(
                ValorPublicacion.ValorPublicacionID,
                ValorPublicacion.TipoPublicacionID,
                ValorPublicacion.ValorBasePublicacion
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar ValorPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public ValorPublicacionRespuesta GetValorPublicacionById(int id)
        {
            ValorPublicacionRespuesta respuesta = new ValorPublicacionRespuesta();
            var info = db.SP_GET_VALORPUBLICACION_BYID(id).FirstOrDefault();

            respuesta.ValorPublicacionID = info.ValorPublicacionID;
            respuesta.TipoPublicacionID = (int)info.TipoPublicacionID;
            respuesta.ValorBasePublicacion = (decimal)info.ValorBasePublicacion;

            return respuesta;
        }

        public ValorPublicacionRespuesta DelValorPublicacion(int id)
        {

            ValorPublicacionRespuesta respuesta = new ValorPublicacionRespuesta();
            try
            {
                var accion = db.SP_DEL_VALORPUBLICACION(
                id


             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar ValorPublicacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }
        #endregion
    }
}