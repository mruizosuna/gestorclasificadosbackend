﻿using GestorClasificados.Models.DTO;
using GestorClasificados.Models.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Utilidades;
namespace GestorClasificados.Services
{

    public class UsuariosService
    {
        GestorClasificadosEntities db = new GestorClasificadosEntities();

        #region ListaPerfilUsuario

        public PerfilUsuarioLista GetListaPerfilUsuario()
        {
            PerfilUsuarioLista respuesta = new PerfilUsuarioLista();
            var lista = db.SP_LIST_PERFILUSUARIO().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public PerfilUsuarioRespuesta AddPerfilUsuario(PerfilUsuarioRespuesta perfilUsuario)
        {

            PerfilUsuarioRespuesta respuesta = new PerfilUsuarioRespuesta();
            try
            {
                var accion = db.SP_ADD_PERFILUSUARIO(

                perfilUsuario.DescripcionPerfil
                );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar EstadosAprobacion -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PerfilUsuarioRespuesta UpdPerfilUsuario(PerfilUsuarioRespuesta perfilUsuario)
        {

            PerfilUsuarioRespuesta respuesta = new PerfilUsuarioRespuesta();
            try
            {
                var accion = db.SP_UPD_PERFILUSUARIO(
                perfilUsuario.PerfilID,
                perfilUsuario.DescripcionPerfil


             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar PerfilUsuario -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public PerfilUsuarioRespuesta GetPerfilUsuarioById(int id)
        {
            PerfilUsuarioRespuesta respuesta = new PerfilUsuarioRespuesta();
            var info = db.SP_GET_PERFILUSUARIO_BYID(id).FirstOrDefault();

            respuesta.PerfilID = info.PerfilID;
            respuesta.DescripcionPerfil = info.DescripcionPerfil;



            return respuesta;
        }

        #endregion

        #region ListaUsuarios

        public UsuariosLista GetListaUsuarios()
        {
            UsuariosLista respuesta = new UsuariosLista();
            var lista = db.SP_LIST_USUARIOS().ToList(); // Cambio aquí
            respuesta.Lista = lista;
            return respuesta;
        }


        public UsuariosRespuesta AddUsuarios(UsuariosRespuesta usuarios)
        {

            UsuariosRespuesta respuesta = new UsuariosRespuesta();
            try
            {
                string contrasena = EncriptarClave(usuarios.Contrasena);
                var accion = db.SP_ADD_USUARIOS(

                usuarios.Nombre,
                usuarios.CorreoElectronico,
                contrasena,
                usuarios.PerfilUsuarioID
                );
                respuesta.OperacionExitosa = true;
                respuesta.Mensaje = "Creación de usuario fue exitoso";
            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Ingresar Usuarios -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public UsuariosRespuesta UpdUsuarios(UsuariosRespuesta usuarios)
        {

            UsuariosRespuesta respuesta = new UsuariosRespuesta();
            try
            {
                string contrasena = EncriptarClave(usuarios.Contrasena);
                var accion = db.SP_UPD_USUARIOS(
                usuarios.UserID,
                usuarios.Nombre,
                usuarios.CorreoElectronico,
                contrasena,
                usuarios.PerfilUsuarioID
             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al Actualizar Usuarios -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        public UsuariosRespuesta GetUsuariosById(int id)
        {
            UsuariosRespuesta respuesta = new UsuariosRespuesta();
            var info = db.SP_GET_USUARIOS_BYID(id).FirstOrDefault();

            respuesta.UserID = info.UserID;
            respuesta.Nombre = info.Nombre;
            respuesta.CorreoElectronico = info.CorreoElectronico;
            respuesta.Contrasena = DesencriptarClave(info.Contrasena);
            respuesta.PerfilUsuarioID = info.PerfilUsuarioID;

            return respuesta;
        }
        public UsuariosRespuesta GetUsuariosByLogin(UsuariosLogin usuariosLogin)
        {
            UsuariosRespuesta respuesta = new UsuariosRespuesta();
            string contrasena = EncriptarClave(usuariosLogin.Contrasena);
            var info = db.SP_GET_USUARIOS_BYLOGIN(usuariosLogin.CorreoElectronico, contrasena).FirstOrDefault();

            if (info != null)
            {
                respuesta.UserID = info.UserID;
                respuesta.Nombre = info.Nombre;
                respuesta.CorreoElectronico = info.CorreoElectronico;
                respuesta.Contrasena = info.Contrasena;
                respuesta.PerfilUsuarioID = info.PerfilUsuarioID;
                respuesta.DescripcionPerfil = info.DescripcionPerfil;
                respuesta.OperacionExitosa = true;
                respuesta.Mensaje = "Autenticación exitosa";
            }
            else
            {
                respuesta.OperacionExitosa = false;
                respuesta.Mensaje = "Autenticación fallida";
            }
            return respuesta;
        }


        public UsuariosRespuesta DelUsuarios(int id)
        {

            UsuariosRespuesta respuesta = new UsuariosRespuesta();
            try
            {
                var accion = db.SP_DEL_USUARIOS(
                id


             );
                respuesta.OperacionExitosa = true;

            }
            catch (Exception ex)
            {
                respuesta.Mensaje = "Error al eliminar Usuarios -" + ex.ToString() + "-";
                respuesta.OperacionExitosa = false;
            }
            return respuesta;
        }

        private String EncriptarClave(String Clave)
        {
            string strClave = Clave;
            string strLlave = "";
            string strClaveEncriptada = "";


            strLlave = Constantes.EncryptionKey;
            FE_Symmetric objSymetric = new FE_Symmetric();
            strClaveEncriptada = objSymetric.DecryptData(strLlave, "84xa3zqaB8E2nblxXcNaWg==");
            strClaveEncriptada = objSymetric.EncryptData(strLlave, strClave);
            return strClaveEncriptada;


        }

        private String DesencriptarClave(String Clave)
        {
            string strClave = Clave;
            string strLlave = "84xa3zqaB8E2nblxXcNaWg==";
            string strClaveDesEncriptada = "";


            strLlave = Constantes.EncryptionKey;
            FE_Symmetric objSymetric = new FE_Symmetric();
            strClaveDesEncriptada = objSymetric.DecryptData(strLlave, strClave);
            //strClaveDesEncriptada = objSymetric.EncryptData(strLlave, strClave);
            return strClaveDesEncriptada;


        }
        #endregion


    }


}