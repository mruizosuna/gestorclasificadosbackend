﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using GestorClasificados.Models.DTO;
using GestorClasificados.Services;
using System.Net.Http;
using System.Net;
using System.Web.Configuration;
using System.IO;
using System.Web;
using System.Configuration;
using GestorClasificados.Models.EDMX;

namespace GestorClasificados.Controllers
{
    [RoutePrefix("puntuaciones")] //Ruta de Acceso al Controlador
    public class PuntuacionesController : ApiController
    {
        private PuntuacionesService srvPuntuaciones;
        private string keyToken = "97221cdc42-8661-4ab9-a04e-51785baa88da39";
        public PuntuacionesController()
        {
            srvPuntuaciones = new PuntuacionesService();
        }



        #region PuntuacionesClasificado

        //Obtener Lista PuntuacionesClasificado
        [Route("getListaPuntuacionesClasificado")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PuntuacionesClasificadoLista))]
        public IHttpActionResult GetListaPuntuacionesClasificado(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPuntuaciones.GetListaPuntuacionesClasificado();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPuntuacionesClasificado" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un PuntuacionesClasificado
        [Route("getPuntuacionesClasificadoById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PuntuacionesClasificadoRespuesta))]
        public Task<IHttpActionResult> GetPuntuacionesClasificadoById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPuntuaciones.GetPuntuacionesClasificadoById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPuntuacionesClasificado" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un PuntuacionesClasificado
        [Route("addPuntuacionesClasificado")]
        [HttpPost]
        [ResponseType(typeof(PuntuacionesClasificadoRespuesta))]
        public IHttpActionResult addAlcance(string token, PuntuacionesClasificadoRespuesta puntuacionesClasificado)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPuntuaciones.AddPuntuacionesClasificado(puntuacionesClasificado);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addPuntuacionesClasificado" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar un PuntuacionesClasificado
        [Route("updPuntuacionesClasificado")]
        [HttpPut]
        [ResponseType(typeof(PuntuacionesClasificadoRespuesta))]
        public IHttpActionResult updPuntuacionesClasificado(string token, PuntuacionesClasificadoRespuesta puntuacionesClasificado)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPuntuaciones.UpdPuntuacionesClasificado(puntuacionesClasificado);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updPuntuacionesClasificado" + e.Message;
                return Ok(error);
            }
        }


        #endregion


        #region PuntuacionesVendedor

        //Obtener Lista PuntuacionesVendedor
        [Route("getListaPuntuacionesVendedor")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PuntuacionesVendedorLista))]
        public IHttpActionResult GetListaPuntuacionesVendedor(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPuntuaciones.GetListaPuntuacionesVendedor();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPuntuacionesVendedor" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un PuntuacionesVendedor
        [Route("getPuntuacionesVendedorById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PuntuacionesVendedorRespuesta))]
        public Task<IHttpActionResult> GetPuntuacionesVendedorById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPuntuaciones.GetPuntuacionesVendedorById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPuntuacionesVendedor" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un PuntuacionesClasificado
        [Route("addPuntuacionesVendedor")]
        [HttpPost]
        [ResponseType(typeof(PuntuacionesVendedorRespuesta))]
        public IHttpActionResult addPuntuacionesVendedor(string token, PuntuacionesVendedorRespuesta puntuacionesVendedor)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPuntuaciones.AddPuntuacionesVendedor(puntuacionesVendedor);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addPuntuacionesVendedor" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar un PuntuacionesVendedor
        [Route("updPuntuacionesVendedor")]
        [HttpPut]
        [ResponseType(typeof(PuntuacionesVendedorRespuesta))]
        public IHttpActionResult updPuntuacionesVendedor(string token, PuntuacionesVendedorRespuesta puntuacionesVendedor)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPuntuaciones.UpdPuntuacionesVendedor(puntuacionesVendedor);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updPuntuacionesVendedor" + e.Message;
                return Ok(error);
            }
        }


        #endregion



        // GET: api/[controller]/getTree
        [Route("test")]
        [HttpGet]
        public string test()
        {
            return "Servidor responde";
        }

    }
}