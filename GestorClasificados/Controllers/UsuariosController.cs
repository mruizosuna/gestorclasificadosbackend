﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using GestorClasificados.Models.DTO;
using GestorClasificados.Services;
using System.Net.Http;
using System.Net;
using System.Web.Configuration;
using System.IO;
using System.Web;
using System.Configuration;
using GestorClasificados.Models.EDMX;
namespace GestorClasificados.Controllers
{
    [RoutePrefix("usuarios")] //Ruta de Acceso al Controlador
    public class UsuariosController : ApiController
    {

        private UsuariosService srvUsuarios;
        private string keyToken = "97221cdc42-8661-4ab9-a04e-51785baa88da39";
        public UsuariosController()
        {
            srvUsuarios = new UsuariosService();
        }


        #region PerfilUsuario

        //Obtener Lista PerfilUsuario
        [Route("getListaPerfilUsuario")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PerfilUsuarioLista))]
        public IHttpActionResult GetListaPerfilUsuario(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvUsuarios.GetListaPerfilUsuario();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPerfilUsuario" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un PerfilUsuario
        [Route("getPerfilUsuarioById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PerfilUsuarioRespuesta))]
        public Task<IHttpActionResult> GetPerfilUsuarioById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvUsuarios.GetPerfilUsuarioById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPerfilUsuario" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un PerfilUsuario
        [Route("addPerfilUsuario")]
        [HttpPost]
        [ResponseType(typeof(PerfilUsuarioRespuesta))]
        public IHttpActionResult addPerfilUsuario(string token, PerfilUsuarioRespuesta perfilUsuario)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvUsuarios.AddPerfilUsuario(perfilUsuario);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addPerfilUsuario" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar un PerfilUsuario
        [Route("updPerfilUsuario")]
        [HttpPut]
        [ResponseType(typeof(PerfilUsuarioRespuesta))]
        public IHttpActionResult updAlcance(string token, PerfilUsuarioRespuesta perfilUsuario)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvUsuarios.UpdPerfilUsuario(perfilUsuario);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updPerfilUsuario" + e.Message;
                return Ok(error);
            }
        }


        #endregion


        #region Usuarios

        //Obtener Lista Usuarios
        [Route("getListaUsuarios")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(UsuariosLista))]
        public IHttpActionResult GetListaUsuarios(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvUsuarios.GetListaUsuarios();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaUsuarios" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un Usuarios
        [Route("getUsuariosById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(UsuariosRespuesta))]
        public Task<IHttpActionResult> GetUsuariosById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvUsuarios.GetUsuariosById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaUsuarios" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Obtener un Usuarios
        [Route("getUsuariosByLogin")] //Ruta de acceso al Metodo
        [HttpPost]  // Tipo de Respuesta
        [ResponseType(typeof(UsuariosRespuesta))]
        public Task<IHttpActionResult> GetUsuariosByLogin(UsuariosLogin usuariosLogin, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvUsuarios.GetUsuariosByLogin(usuariosLogin);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaUsuarios" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un Usuarios
        [Route("addUsuarios")]
        [HttpPost]
        [ResponseType(typeof(UsuariosRespuesta))]
        public IHttpActionResult addUsuarios(string token, UsuariosRespuesta usuarios)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvUsuarios.AddUsuarios(usuarios);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addUsuarios" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar un PerfilUsuario
        [Route("updUsuarios")]
        [HttpPut]
        [ResponseType(typeof(UsuariosRespuesta))]
        public IHttpActionResult updUsuarios(string token, UsuariosRespuesta usuarios)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvUsuarios.UpdUsuarios(usuarios);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updUsuarios" + e.Message;
                return Ok(error);
            }
        }

        //Eliminar Usuarios
        [Route("delUsuarios")]
        [HttpGet]
        [ResponseType(typeof(UsuariosRespuesta))]
        public IHttpActionResult delUsuarios(int id, string token)
        //public IHttpActionResult delUsuarios(int id)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvUsuarios.DelUsuarios(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio delUsuarios" + e.Message;
                return Ok(error);
            }
        }


        #endregion


        // GET: api/[controller]/getTree
        [Route("test")]
        [HttpGet]
        public string test()
        {
            return "Servidor responde";
        }
    }
}