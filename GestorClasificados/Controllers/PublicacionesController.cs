﻿using GestorClasificados.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Description;
using System.Web.Http;
using GestorClasificados.Services;
using GestorClasificados.Models.EDMX;
using System.Configuration;
using System.Net.Http;
using System.Globalization;
using System.Net;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;
using System.Web.UI;
using Newtonsoft.Json.Linq;

namespace GestorClasificados.Controllers
{
    [RoutePrefix("publicaciones")] //Ruta de Acceso al Controlador
    public class PublicacionesController:ApiController
    {

        private PublicacionesService srvPublicaciones;
        private ParametricasService srvParametricas;
        private string keyToken = "97221cdc42-8661-4ab9-a04e-51785baa88da39";
        public PublicacionesController()
        {
            srvPublicaciones = new PublicacionesService();
            srvParametricas = new ParametricasService();
        }

        #region ImagenesPublicacion

        //Obtener Lista ImagenesPublicacion
        [Route("getListaImagenesPublicacion")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(ImagenesPublicacionLista))]
        public IHttpActionResult GetImagenesPublicacion(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaImagenesPublicacion(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaImagenesPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un ImagenesPublicacion
        [Route("getImagenesPublicacionById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(ImagenesPublicacionRespuesta))]
        public Task<IHttpActionResult> GetImagenesPublicacionById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetImagenesPublicacionById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetImagenesPublicacionById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

       
        //Nuevo Metodo
        //Adicionar un Conjunto
        [Route("addImagenesPublicacion")]
        [HttpPost]
        [ResponseType(typeof(ImagenesPublicacionRespuesta))]
        public IHttpActionResult addImagenesPublicacion(string token, ImagenesPublicacionRespuesta imagenesPublicacion)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.AddImagenesPublicacionAsync(imagenesPublicacion);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addImagenesPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar una ZonasLocalidad
        [Route("updImagenesPublicacion")]
        [HttpPut]
        [ResponseType(typeof(ImagenesPublicacionRespuesta))]
        public IHttpActionResult updImagenesPublicacion(string token, ImagenesPublicacionRespuesta imagenesPublicacion)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.UpdImagenesPublicacion(imagenesPublicacion);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updImagenesPublicacion" + e.Message;
                return Ok(error);
            }
        }



        [Route("delImagenPublicacion")]
        [HttpGet]
        [ResponseType(typeof(ImagenesPublicacionRespuesta))]
        public IHttpActionResult delImagenPublicacion(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.DelImagenPublicacionById(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio DelImagenPublicacion" + e.Message;
                return Ok(error);
            }
        }
        #endregion

        #region VideosPublicacion

        //Obtener Lista VideosPublicacion
        [Route("getListaVideosPublicacion")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(VideosPublicacionLista))]
        public IHttpActionResult GetListaVideosPublicacion(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaVideosPublicacion();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaVideosPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Obtener Lista VideosPublicacion
        [Route("getListaVideosPublicacionById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(VideosPublicacionByIdLista))]
        public IHttpActionResult GetListaVideosPublicacionById(int id,string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaVideosPublicacionById(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaVideosPublicacionById" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un ValorPublicacion
        [Route("getVideosPublicacionById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(VideosPublicacionRespuesta))]
        public Task<IHttpActionResult> GetVideosPublicacionById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetVideosPublicacionById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetVideosPublicacionById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un ValorPublicacion
        [Route("addVideosPublicacion")]
        [HttpPost]
        [ResponseType(typeof(VideosPublicacionRespuesta))]
        public IHttpActionResult addVideosPublicacion(string token, VideosPublicacionRespuesta videosPublicacion)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.AddVideosPublicacion(videosPublicacion);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addVideosPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar una ValorPublicacion
        [Route("updVideosPublicacion")]
        [HttpPut]
        [ResponseType(typeof(ValorPublicacionRespuesta))]
        public IHttpActionResult updVideosPublicacion(string token, VideosPublicacionRespuesta videosPublicacion)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.UpdVideosPublicacion(videosPublicacion);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updVideosPublicacion" + e.Message;
                return Ok(error);
            }
        }

        [Route("delVideoPublicacion")]
        [HttpGet]
        [ResponseType(typeof(VideosPublicacionRespuesta))]
        public IHttpActionResult delVideoPublicacion(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.DelVideoPublicacionById(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio DelVideoPublicacion" + e.Message;
                return Ok(error);
            }
        }


        #endregion

        #region PublicacionSubCategorias

        //Obtener Lista PublicacionSubCategorias
        [Route("getListaPublicacionSubCategorias")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PublicacionSubCategoriasLista))]
        public IHttpActionResult GetListaPublicacionSubCategorias(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaPublicacionSubCategorias();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPublicacionSubCategorias" + e.Message;
                return Ok(error);
            }
        }


        //Obtener Lista PublicacionSubCategorias
        [Route("getListaPublicacionSubCategoriasById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PublicacionSubCategoriasLista))]
        public IHttpActionResult GetListaPublicacionSubCategoriasById(int id,string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaPublicacionSubCategoriasById(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPublicacionSubCategorias" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un Publicaciones
        [Route("getPublicacionSubCategoriasById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PublicacionSubCategoriasRespuesta))]
        public Task<IHttpActionResult> GetPublicacionSubCategoriasById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetPublicacionSubCategoriasById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetPublicacionSubCategoriasById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un Publicaciones
        [Route("addPublicacionSubCategorias")]
        [HttpPost]
        [ResponseType(typeof(PublicacionSubCategoriasRespuesta))]
        public IHttpActionResult addPublicacionSubCategorias(string token, PublicacionSubCategoriasRespuesta publicacionSubCategorias)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.AddPublicacionSubCategorias(publicacionSubCategorias);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addPublicacionSubCategorias" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar una PublicacionSubCategorias
        [Route("updPublicacionSubCategorias")]
        [HttpPut]
        [ResponseType(typeof(PublicacionSubCategoriasRespuesta))]
        public IHttpActionResult updPublicacionSubCategorias(string token, PublicacionSubCategoriasRespuesta publicacionSubCategorias)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.UpdPublicacionSubCategorias(publicacionSubCategorias);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updPublicacionSubCategorias" + e.Message;
                return Ok(error);
            }
        }

        [Route("delPublicacionSubCategorias")]
        [HttpGet]
        [ResponseType(typeof(PublicacionSubCategoriasRespuesta))]
        public IHttpActionResult delPublicacionSubCategorias(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.DelCategoriaPublicacionById(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio DelVideoPublicacion" + e.Message;
                return Ok(error);
            }
        }

        #endregion

        #region PublicacionesConjuntos

        //Obtener Lista PublicacionesConjuntos
        [Route("getListaPublicacionesConjuntos")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PublicacionesConjuntosLista))]
        public IHttpActionResult GetListaPublicacionesConjuntos(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaPublicacionesConjuntos();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPublicacionesConjuntos" + e.Message;
                return Ok(error);
            }
        }


        //Obtener Lista PublicacionesConjuntos
        [Route("getListaPublicacionesConjuntosById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PublicacionesConjuntosByIdLista))]
        public IHttpActionResult GetListaPublicacionesConjuntosById(int id,string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaPublicacionesConjuntosById(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPublicacionesConjuntosById" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un PublicacionesConjuntos
        [Route("getPublicacionesConjuntosById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PublicacionesConjuntosRespuesta))]
        public Task<IHttpActionResult> GetPublicacionesConjuntosById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetPublicacionesConjuntosById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetPublicacionesConjuntosById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un PublicacionesConjuntos
        [Route("addPublicacionesConjuntos")]
        [HttpPost]
        [ResponseType(typeof(PublicacionesConjuntosRespuesta))]
        public IHttpActionResult addVideosPublicacion(string token, PublicacionesConjuntosRespuesta publicacionesConjuntos)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.AddPublicacionesConjuntos(publicacionesConjuntos);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addPublicacionesConjuntos" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar una PublicacionesConjuntos
        [Route("updPublicacionesConjuntos")]
        [HttpPut]
        [ResponseType(typeof(PublicacionesConjuntosRespuesta))]
        public IHttpActionResult updPublicacionesConjuntos(string token, PublicacionesConjuntosRespuesta publicacionesConjuntos)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.UpdPublicacionesConjuntos(publicacionesConjuntos);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updPublicacionesConjuntos" + e.Message;
                return Ok(error);
            }
        }

        [Route("delPublicacionConjuntos")]
        [HttpGet]
        [ResponseType(typeof(PublicacionesConjuntosRespuesta))]
        public IHttpActionResult delPublicacionConjuntos(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.DelConjuntoPublicacionById(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio DelConjuntoPublicacion" + e.Message;
                return Ok(error);
            }
        }
        #endregion


        #region Publicaciones

        //Obtener Lista PeriodosPublicacion
        [Route("getListaPublicaciones")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PublicacionesLista))]
        public IHttpActionResult GetPublicacionesLista(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaPublicaciones();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPublicaciones" + e.Message;
                return Ok(error);
            }
        }

        //Obtener Lista PeriodosPublicacion
        [Route("getListaPublicacionesByUser")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PublicacionesListaUsuario))]
        public IHttpActionResult GetPublicacionesListaByUser(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaPublicacionesByUser(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPublicacionesByUser" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un Publicaciones
        [Route("getPublicacionesById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PublicacionesRespuesta))]
        public Task<IHttpActionResult> GetPublicacionesById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetPublicacionesById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetPublicacionesById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un Publicaciones
        [Route("addPublicaciones")]
        [HttpPost]
        [ResponseType(typeof(PublicacionesRespuesta))]
        public IHttpActionResult addPublicaciones(string token, PublicacionesRespuesta publicaciones)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.AddPublicaciones(publicaciones);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addPublicaciones" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar una Publicaciones
        [Route("updPublicaciones")]
        [HttpPut]
        [ResponseType(typeof(PublicacionesRespuesta))]
        public IHttpActionResult updPublicaciones(string token, PublicacionesRespuesta publicaciones)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.UpdPublicaciones(publicaciones);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updPublicaciones" + e.Message;
                return Ok(error);
            }
        }



        //Actualizar una Publicaciones
        [Route("updPublicacioestado")]
        [HttpPut]
        [ResponseType(typeof(PublicacionesRespuesta))]
        public IHttpActionResult updPublicacionEstado(string token, PublicacionesRespuesta publicaciones)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.UpdPublicacionEstado(publicaciones);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updPublicaciones" + e.Message;
                return Ok(error);
            }
        }

    

        [Route("delPublicacion")]
        [HttpGet]
        [ResponseType(typeof(PublicacionesRespuesta))]
        public IHttpActionResult delPublicacion(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.DelPublicacionById(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio DelPublicacion" + e.Message;
                return Ok(error);
            }
        }



        //Buscador de Publicaciones

        [Route("getListaBuscadorPublicacion")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PublicacionesBuscadorLista))]
        public IHttpActionResult GetListaBuscadorPublicaciones(int categoriaid, int subcategoriaid, string zona, string palabrasclave, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaBuscadorPublicaciones(categoriaid, subcategoriaid, zona,palabrasclave);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPublicadorPublicacion" + e.Message;
                return Ok(error);
            }
        }
        #endregion

        /*PARAMETRICAS */

        #region TiposPublicacion

        //Obtener Lista TiposPublicacion
        [Route("getListaTiposPublicacion")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(TiposPublicacionLista))]
        public IHttpActionResult GetListaTiposPublicacion(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaTiposPublicacion();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaTiposPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un ValorPublicacion
        [Route("getTiposPublicacionById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(TiposPublicacionRespuesta))]
        public Task<IHttpActionResult> GetTiposPublicacionById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetTiposPublicacionById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetTiposPublicacionById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un TiposPublicacion
        [Route("addTiposPublicacion")]
        [HttpPost]
        [ResponseType(typeof(TiposPublicacionRespuesta))]
        public IHttpActionResult addTiposPublicacion(string token, TiposPublicacionRespuesta tiposPublicacion)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.AddTiposPublicacion(tiposPublicacion);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addTiposPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar una TiposPublicacion
        [Route("updTiposPublicacion")]
        [HttpPut]
        [ResponseType(typeof(TiposPublicacionRespuesta))]
        public IHttpActionResult updTiposPublicacion(string token, TiposPublicacionRespuesta tiposPublicacion)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.UpdTiposPublicacion(tiposPublicacion);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updTiposPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Eliminar CategoriasPrincipales
        [Route("delTiposPublicacion")]
        [HttpGet]
        [ResponseType(typeof(TiposPublicacionRespuesta))]
        public IHttpActionResult delTiposPublicacion(int id, string token)
        //public IHttpActionResult delTiposPublicacion(int id)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.DelTiposPublicacion(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio delTiposPublicacion" + e.Message;
                return Ok(error);
            }
        }
        #endregion

        #region PeriodosPublicacion

        //Obtener Lista PeriodosPublicacion
        [Route("getListaPeriodosPublicacion")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PeriodosPublicacionLista))]
        public IHttpActionResult GetPeriodosPublicacionLista(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaPeriodosPublicacion();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPeriodosPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un PeriodosPublicacion
        [Route("getPeriodosPublicacionById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PeriodosPublicacionRespuesta))]
        public Task<IHttpActionResult> GetPeriodosPublicacionById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetPeriodosPublicacionById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetPeriodosPublicacionById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un Conjunto
        [Route("addPeriodosPublicacion")]
        [HttpPost]
        [ResponseType(typeof(PeriodosPublicacionRespuesta))]
        public IHttpActionResult addPeriodosPublicacion(string token, PeriodosPublicacionRespuesta periodosPublicacion)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.AddPeriodosPublicacion(periodosPublicacion);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addPeriodosPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar una PeriodosPublicacion
        [Route("updPeriodosPublicacion")]
        [HttpPut]
        [ResponseType(typeof(PeriodosPublicacionRespuesta))]
        public IHttpActionResult updPeriodosPublicacion(string token, PeriodosPublicacionRespuesta periodosPublicacion)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.UpdPeriodosPublicacion(periodosPublicacion);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updPeriodosPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Eliminar Periodos de Publicacion
        [Route("delPeriodosPublicacion")]
        [HttpGet]
        [ResponseType(typeof(PeriodosPublicacionRespuesta))]
        public IHttpActionResult delPeriodosPublicacion(int id, string token)
        //public IHttpActionResult delCategoriasPrincipales(int id)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.DelPeriodosPublicacion(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio delCategoriasPrincipales" + e.Message;
                return Ok(error);
            }
        }

        #endregion

        #region ValorPublicacion

        //Obtener Lista ValorPublicacion
        [Route("getListaValorPublicacion")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(ValorPublicacionLista))]
        public IHttpActionResult GetListaValorPublicacion(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetListaValorPublicacion();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaValorPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un ValorPublicacion
        [Route("getValorPublicacionById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(ValorPublicacionRespuesta))]
        public Task<IHttpActionResult> GetValorPublicacionById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.GetValorPublicacionById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetValorPublicacionById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un ValorPublicacion
        [Route("addValorPublicacion")]
        [HttpPost]
        [ResponseType(typeof(ValorPublicacionRespuesta))]
        public IHttpActionResult addValorPublicacion(string token, ValorPublicacionRespuesta valorPublicacion)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.AddValorPublicacion(valorPublicacion);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addValorPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar una ValorPublicacion
        [Route("updValorPublicacion")]
        [HttpPut]
        [ResponseType(typeof(ValorPublicacionRespuesta))]
        public IHttpActionResult updValorPublicacion(string token, ValorPublicacionRespuesta valorPublicacion)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.UpdValorPublicacion(valorPublicacion);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updValorPublicacion" + e.Message;
                return Ok(error);
            }
        }

        //Eliminar ValorPublicacion
        [Route("delValorPublicacion")]
        [HttpGet]
        [ResponseType(typeof(ValorPublicacionRespuesta))]
        public IHttpActionResult delValorPublicacion(int id, string token)
        //public IHttpActionResult delValorPublicacion(int id)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvPublicaciones.DelValorPublicacion(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio delValorPublicacion" + e.Message;
                return Ok(error);
            }
        }
        #endregion

        // GET: api/[controller]/getTree
        [Route("test")]
        [HttpGet]
        public string test()
        {
            return "Servidor responde";
        }


        // Nueva acción para recibir notificaciones de PayU
        [Route("confirmacion-payu")]
        [HttpGet, HttpPost]
        public async Task<IHttpActionResult> ConfirmacionPayUAsync()
        {
            PayUConfirmation confirmation;

            if (Request.Method == HttpMethod.Get)
            {
                JObject auditData = new JObject();
                // Iterar sobre el query string para añadir cada par clave-valor al JObject
                foreach (string key in HttpContext.Current.Request.QueryString)
                {
                    auditData[key] = HttpContext.Current.Request.QueryString[key];
                }

                // Convertir el JObject a una cadena JSON
                string auditJson = auditData.ToString();

                // Procesar los datos desde la URL en una solicitud GET

                string extra3 = HttpContext.Current.Request.QueryString["extra3"];
                string name = null;
                string phoneNumber = null;

                if (!string.IsNullOrEmpty(extra3))
                {
                    string[] values = extra3.Split('/');
                    if (values.Length == 2)
                    {
                        name = values[0];
                        phoneNumber = values[1];
                    }
                }

                confirmation = new PayUConfirmation
                {
                    MerchantId = int.Parse(HttpContext.Current.Request.QueryString["merchantId"]),
                    Description = HttpContext.Current.Request.QueryString["description"],
                    ReferenceSale = HttpContext.Current.Request.QueryString["referenceCode"],
                    Value = decimal.Parse(HttpContext.Current.Request.QueryString["TX_VALUE"].Replace(',', '.'), CultureInfo.InvariantCulture),
                    Extra1 = HttpContext.Current.Request.QueryString["extra1"],
                    Extra2 = HttpContext.Current.Request.QueryString["extra2"],
                    Extra3 = name,
                    Telefono = phoneNumber,
                    Email = HttpContext.Current.Request.QueryString["buyerEmail"],
                    Currency = HttpContext.Current.Request.QueryString["currency"],
                    StatePol = int.Parse(HttpContext.Current.Request.QueryString["polTransactionState"]),
                    Sign = HttpContext.Current.Request.QueryString["signature"],
                    TrazabilityCode = HttpContext.Current.Request.QueryString["trazabilityCode"],
                    Auditoria = auditJson // Asignar el JSON al campo Auditoria

                };
            }
            else if (Request.Method == HttpMethod.Post)
            {
                // Procesar los datos desde el cuerpo de la solicitud en una solicitud POST
                confirmation = Request.Content.ReadAsAsync<PayUConfirmation>().Result;
            }
            else
            {
                return BadRequest("Método HTTP no soportado.");
            }

            if (confirmation == null)
            {
                return BadRequest("Datos de confirmación no proporcionados");
            }

            try
            {
                // Validar la firma si es necesario
                string apiKey = ConfigurationManager.AppSettings["APIKey"];
                string merchantId = confirmation.MerchantId.ToString();
                string referenceSale = confirmation.ReferenceSale;
                string value = confirmation.Value.ToString("0.##")+".0"; // Formatear valor según los decimales especificados
                string currency = confirmation.Currency;
                string statePol = confirmation.StatePol.ToString();
                string tipoDocumento = confirmation.Extra1.ToString();
                string numeroDocumento = confirmation.Extra2.ToString();
                string razonSocial = confirmation.Extra3;
                string Auditoria = confirmation.Auditoria;

                string stringToHash = $"{apiKey}~{merchantId}~{referenceSale}~{value}~{currency}~{statePol}";
                string hash = CreateMD5Hash(stringToHash);

                if (hash != confirmation.Sign)
                {
                    return BadRequest("Firma inválida");
                }

                // Procesar los datos recibidos, como guardarlos en la base de datos
                Console.WriteLine("Datos recibidos de PayU:", confirmation);
                //Aqui debo invocar el procesamiento de pago

                PagosRespuesta pagos = new PagosRespuesta
                {
                    PublicacionID = int.Parse(new string(confirmation.ReferenceSale.Where(char.IsDigit).ToArray())),
                    FechaPago = DateTime.Now,
                    MontoaPagar = confirmation.Value,
                    Descuentos = 0,
                    MontoPagado = confirmation.Value,
                    NumeroReferencia = confirmation.ReferenceSale +"  /  "+ confirmation.Description,
                    TipoPago = 1, //Payu
                    Estado = confirmation.StatePol == 4 ? 2 : 1 ,// 2 Pagado
                    TelefonoCelular = confirmation.Telefono,
                    Email = confirmation.Email,
                    RazonSocial = confirmation.Extra3,
                    TipoDocumentoID = Convert.ToInt32(confirmation.Extra1),
                    NumeroDocumento = confirmation.Extra2,
                    Auditoria = confirmation.Auditoria
                    
                };
             

                var respuesta = srvParametricas.AddPagos(pagos);


                if(confirmation.StatePol == 4) //Si el pago quedó aprobado, se actualiza automaticamente para ser visible la publicación
                {
                    PublicacionesRespuesta publicacion = new PublicacionesRespuesta
                    {
                        PublicacionID = int.Parse(new string(confirmation.ReferenceSale.Where(char.IsDigit).ToArray())),
                        EstadoAprobacionID = 2, //Aprobado
                        PagoRealizado = true //Pagado
                    };

                    var actualizacionEstado = srvPublicaciones.UpdPublicacionEstado(publicacion);
                }


                // Responder a PayU para confirmar que recibiste los datos
                //  return Ok("Confirmación recibida");

                //Una vez actualizado el Pago se redirige a la página de la Publicación

                string redirectUrl = ConfigurationManager.AppSettings["UrlReenvio"];
                var response = Request.CreateResponse(HttpStatusCode.Redirect);
                response.Headers.Location = new Uri(redirectUrl);
                return ResponseMessage(response);
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio confirmacion-payu: " + e.Message;
                return InternalServerError(new Exception(error));
            }
        }

        // Método para crear hash MD5
        private static string CreateMD5Hash(string input)
        {
            using (var md5 = System.Security.Cryptography.MD5.Create())
            {
                var inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                var hashBytes = md5.ComputeHash(inputBytes);
                var sb = new System.Text.StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString().ToLower();
            }
        }

        // Clase PayUConfirmation para recibir los datos
        public class PayUConfirmation
        {
            public int MerchantId { get; set; }
            public string ReferenceSale { get; set; }
            public string Description { get; set; } 
            public decimal Value { get; set; }
            public string Currency { get; set; }
            public int StatePol { get; set; }
            public string Sign { get; set; }

            public string Extra1 { get; set; }
            public string Extra2 { get; set; }
            public string Extra3 { get; set; }

            public string Email { get; set; }   
            public string Telefono {  get; set; }   

            public string Auditoria {  get; set; }  
            public string TrazabilityCode { get; set; }
            
        }


    }
}

//Comentario de prueba
//Comentario EMRO 18 03 2024 522 pm