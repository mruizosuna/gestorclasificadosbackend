﻿using GestorClasificados.Models.DTO;
using GestorClasificados.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Description;
using System.Web.Http;
using GestorClasificados.Models.EDMX;

namespace GestorClasificados.Controllers
{
    [RoutePrefix("categorias")] //Ruta de Acceso al Controlador
    public class CategoriasController : ApiController
    {
        private CategoriasService srvCategorias;
        private string keyToken = "97221cdc42-8661-4ab9-a04e-51785baa88da39";
        public CategoriasController()
        {
            srvCategorias = new CategoriasService();
        }



        #region CategoriasPrincipales

        //Obtener Lista CategoriasPrincipales
        [Route("getListaCategoriasPrincipales")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(CategoriasPrincipalesLista))]
        public IHttpActionResult GetListaCategoriasPrincipales(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvCategorias.GetListaCategoriasPrincipales();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaCategoriasPrincipales" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un CategoriasPrincipales
        [Route("getCategoriasPrincipalesById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(CategoriasPrincipalesRespuesta))]
        public Task<IHttpActionResult> GeCategoriasPrincipalesById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvCategorias.GetCategoriasPrincipalesById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaCategoriasPrincipales" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un CategoriasPrincipales
        [Route("addCategoriasPrincipales")]
        [HttpPost]
        [ResponseType(typeof(CategoriasPrincipalesRespuesta))]
        public IHttpActionResult addCategoriasPrincipales(string token, CategoriasPrincipalesRespuesta categoriasPrincipales)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvCategorias.AddCategoriasPrincipales(categoriasPrincipales);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addCategoriasPrincipales" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar un CategoriasPrincipales
        [Route("updCategoriasPrincipales")]
        [HttpPut]
        [ResponseType(typeof(CategoriasPrincipalesRespuesta))]
        public IHttpActionResult updAlcance(string token, CategoriasPrincipalesRespuesta categoriasPrincipales)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvCategorias.UpdCategoriasPrincipales(categoriasPrincipales);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updCategoriasPrincipales" + e.Message;
                return Ok(error);
            }
        }

        //Eliminar CategoriasPrincipales
        [Route("delCategoriasPrincipales")]
        [HttpGet]
        [ResponseType(typeof(CategoriasPrincipalesRespuesta))]
        public IHttpActionResult delCategoriasPrincipales(int id, string token)
        //public IHttpActionResult delCategoriasPrincipales(int id)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvCategorias.DelCategoriasPrincipales(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio delCategoriasPrincipales" + e.Message;
                return Ok(error);
            }
        }

        #endregion


        #region CategoriasSecundarias

        //Obtener Lista CategoriasSecundarias
        [Route("getListaCategoriasSecundarias")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(CategoriasSecundariasLista))]
        public IHttpActionResult GetListaCategoriasSecundarias(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvCategorias.GetListaCategoriasSecundarias();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaCategoriasSecundarias" + e.Message;
                return Ok(error);
            }
        }


        //Obtener Lista CategoriasSecundarias
        [Route("getListaCategoriasSecundariasById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(CategoriasSecundariasByIdLista))]
        public IHttpActionResult GetListaCategoriasSecundariasById(int id,string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvCategorias.GetListaCategoriasSecundariasById(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaCategoriasSecundariasById" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un Conjunto
        [Route("getCategoriasSecundariasById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(CategoriasSecundariasRespuesta))]
        public Task<IHttpActionResult> GetCategoriasSecundariasById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvCategorias.GetCategoriasSecundariasById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetCategoriasSecundariasById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un CategoriasSecundarias
        [Route("addCategoriasSecundarias")]
        [HttpPost]
        [ResponseType(typeof(CategoriasSecundariasRespuesta))]
        public IHttpActionResult addCategoriasSecundarias(string token, CategoriasSecundariasRespuesta categoriasSecundarias)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvCategorias.AddCategoriasSecundarias(categoriasSecundarias);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addCategoriasSecundarias" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar un CategoriasSecundarias
        [Route("updCategoriasSecundarias")]
        [HttpPut]
        [ResponseType(typeof(CategoriasSecundariasRespuesta))]
        public IHttpActionResult updCategoriasSecundarias(string token, CategoriasSecundariasRespuesta categoriasSecundarias)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvCategorias.UpdCategoriasSecundarias(categoriasSecundarias);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updCategoriasSecundarias" + e.Message;
                return Ok(error);
            }
        }


        #endregion


        // GET: api/[controller]/getTree
        [Route("test")]
        [HttpGet]
        public string test()
        {
            return "Servidor responde";
        }

        //Comentario previo a versionamiento 18/03/2024 4:36pm
        //COmentario prueba
    }
}