﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using GestorClasificados.Models.DTO;
using GestorClasificados.Services;
using System.Net.Http;
using System.Net;
using System.Web.Configuration;
using System.IO;
using System.Web;
using System.Configuration;
using GestorClasificados.Models.EDMX;

namespace GestorClasificados.Controllers
{
    [RoutePrefix("parametricas")] //Ruta de Acceso al Controlador
    public class ParametricasController : ApiController
    {
        private ParametricasService srvParametricas;
        private string keyToken = "97221cdc42-8661-4ab9-a04e-51785baa88da39";
        public ParametricasController()
        {
            srvParametricas = new ParametricasService();
        }



        #region Alcances

        //Obtener Lista Alcances
        [Route("getListaAlcances")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(AlcanceLista))]
        public IHttpActionResult GetListaAlcances(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.GetListaAlcances();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaAlcances" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un Alcance
        [Route("getAlcanceById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(AlcanceRespuesta))]
        public Task<IHttpActionResult> GetAlcanceById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.GetAlcanceById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaAlcances" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un Alcance
        [Route("addAlcance")]
        [HttpPost]
        [ResponseType(typeof(AlcanceRespuesta))]
        public IHttpActionResult addAlcance(string token, AlcanceRespuesta alcance)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.AddAlcance(alcance);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addAlcance" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar un Alcance
        [Route("updAlcance")]
        [HttpPut]
        [ResponseType(typeof(AlcanceRespuesta))]
        public IHttpActionResult updAlcance(string token, AlcanceRespuesta alcance)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.UpdAlcance(alcance);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updAlcance" + e.Message;
                return Ok(error);
            }
        }

        //Eliminar Alcance
        [Route("delAlcance")]
        [HttpGet]
        [ResponseType(typeof(AlcanceRespuesta))]
        public IHttpActionResult delAlcance(int id, string token)
        //public IHttpActionResult delAlcance(int id)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.DelAlcance(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio delZonasLocalidad" + e.Message;
                return Ok(error);
            }
        }


        #endregion


        #region Conjuntos

        //Obtener Lista Alcances
        [Route("getListaConjuntos")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(ConjuntosLista))]
        public IHttpActionResult GetListaConjuntos(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.GetListaConjuntos();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaConjuntos" + e.Message;
                return Ok(error);
            }
        }

        //Obtener Lista Alcances
        [Route("getListaConjuntosById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(ConjuntosByIdLista))]
        public IHttpActionResult GetListaConjuntosById(int id,string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.GetListaConjuntosById(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaConjuntosById" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un Conjunto
        [Route("getConjuntosById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(ConjuntosRespuesta))]
        public Task<IHttpActionResult> GetConjuntosById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.GetConjuntosById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetConjuntosById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un Conjunto
        [Route("addConjuntos")]
        [HttpPost]
        [ResponseType(typeof(ConjuntosRespuesta))]
        public IHttpActionResult addConjuntos(string token, ConjuntosRespuesta conjuntos)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.AddConjuntos(conjuntos);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addConjuntos" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar un Conjunto
        [Route("updConjuntos")]
        [HttpPut]
        [ResponseType(typeof(ConjuntosRespuesta))]
        public IHttpActionResult updConjuntos(string token, ConjuntosRespuesta conjuntos)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.UpdConjuntos(conjuntos);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updConjuntos" + e.Message;
                return Ok(error);
            }
        }

        //Eliminar Conjunto
        [Route("delConjuntos")]
        [HttpGet]
        [ResponseType(typeof(ConjuntosRespuesta))]
        public IHttpActionResult delConjuntos(int id, string token)
        //public IHttpActionResult delConjuntos(int id)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.DelConjuntos(id);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio delConjuntos" + e.Message;
                return Ok(error);
            }
        }

            #endregion


        #region EstadosAprobacion

            //Obtener Lista EstadosAprobacion
            [Route("getListaEstadosAprobacion")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(EstadosAprobacionLista))]
        public IHttpActionResult GetEstadosAprobacion(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.GetListaEstadosAprobacion();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaEstadosAprobacion" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un Alcance
        [Route("getEstadosAprobacionById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(EstadosAprobacionRespuesta))]
        public Task<IHttpActionResult> GetEstadosAprobacionById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.GetEstadosAprobacionById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaEstadosAprobacion" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un Alcance
        [Route("addEstadosAprobacion")]
        [HttpPost]
        [ResponseType(typeof(EstadosAprobacionRespuesta))]
        public IHttpActionResult addEstadosAprobacion(string token, EstadosAprobacionRespuesta estadosAprobacion)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.AddEstadosAprobacion(estadosAprobacion);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addEstadosAprobacion" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar un Alcance
        [Route("updEstadosAprobacion")]
        [HttpPut]
        [ResponseType(typeof(EstadosAprobacionRespuesta))]
        public IHttpActionResult updEstadosAprobacion(string token, EstadosAprobacionRespuesta estadosAprobacion)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.UpdEstadosAprobacion(estadosAprobacion);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updEstadosAprobacion" + e.Message;
                return Ok(error);
            }
        }


        #endregion


        #region Pagos
        //Modificado
        //Obtener Lista Pagos
        [Route("getListaPagos")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PagosLista))]
        public IHttpActionResult GetListaPagos(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.GetListaPagos();
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaPagos" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un Pago
        [Route("getPagosById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(PagosRespuesta))]
        public Task<IHttpActionResult> GetPagosById(int id, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.GetPagosById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetPagosById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un Pago
        [Route("addPagos")]
        [HttpPost]
        [ResponseType(typeof(PagosRespuesta))]
        public IHttpActionResult addPagos(string token, PagosRespuesta pagos)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.AddPagos(pagos);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addPagos" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar un Pago
        [Route("updPagos")]
        [HttpPut]
        [ResponseType(typeof(PagosRespuesta))]
        public IHttpActionResult updPagos(string token, PagosRespuesta pagos)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.UpdPagos(pagos);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updPagos" + e.Message;
                return Ok(error);
            }
        }


        //Obtener un Descuento
        [Route("getDescuentoById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(DescuentoRespuesta))]
        public Task<IHttpActionResult> GetDescuentoById(string id, int userid, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.GetDescuentoById(id, userid);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetDescuentoById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }


        //Actualizar un Pago
        [Route("updDescuento")]
        [HttpPut]
        [ResponseType(typeof(PagosRespuesta))]
        public IHttpActionResult updDescuento(string token, DescuentoRespuesta descuento)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.UpdDescuento(descuento);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updDescuento" + e.Message;
                return Ok(error);
            }
        }
        #endregion


        #region ZonasLocalidad

        //Obtener Lista ZonasLocalidad
        [Route("getListaZonasLocalidad")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(ZonasLocalidadLista))]
        public IHttpActionResult GetListaZonasLocalidad(string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.GetListaZonasLocalidad();
                    return Ok(respuesta);
                }
                else
                  return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaZonasLocalidad" + e.Message;
                return Ok(error);
            }
        }

        //Obtener un ZonasLocalidad
        [Route("getZonasLocalidadById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(ZonasLocalidadRespuesta))]

        public Task<IHttpActionResult> GetZonasLocalidadById(int id, string token)
        //public Task<IHttpActionResult> GetZonasLocalidadById(int id)
        {
            try
            {
               if (token.Equals(keyToken))
                {
                    
                    var respuesta = srvParametricas.GetZonasLocalidadById(id);
                    return Task.FromResult<IHttpActionResult>(Ok(respuesta));
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetZonasLocalidadById" + e.Message;
                return Task.FromResult<IHttpActionResult>(Ok(error));
            }
        }

        //Adicionar un Conjunto
        [Route("addZonasLocalidad")]
        [HttpPost]
        [ResponseType(typeof(ZonasLocalidadRespuesta))]
        public IHttpActionResult addZonasLocalidad(ZonasLocalidadRespuesta zonasLocalidad,string token)
        //public IHttpActionResult addZonasLocalidad( ZonasLocalidadRespuesta zonasLocalidad)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.AddZonasLocalidad(zonasLocalidad);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio addZonasLocalidad" + e.Message;
                return Ok(error);
            }
        }

        //Actualizar una ZonasLocalidad
        [Route("updZonasLocalidad")]
        [HttpPut]
        [ResponseType(typeof(ZonasLocalidadRespuesta))]
        public IHttpActionResult updZonasLocalidad(ZonasLocalidadRespuesta zonasLocalidad, string token)
        //public IHttpActionResult updZonasLocalidad(ZonasLocalidadRespuesta zonasLocalidad)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.UpdZonasLocalidad(zonasLocalidad);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio updZonasLocalidad" + e.Message;
                return Ok(error);
            }
        }


        //Eliminar Zona localidad
        [Route("delZonasLocalidad")]
        [HttpGet]
        [ResponseType(typeof(ZonasLocalidadRespuesta))]
        public IHttpActionResult delZonasLocalidad( int id, string token)
        //public IHttpActionResult delZonasLocalidad(int id)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                var respuesta = srvParametricas.DelZonasLocalidad(id);
                return Ok(respuesta);
                 }
                else
                 return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio delZonasLocalidad" + e.Message;
                return Ok(error);
            }
        }

        #endregion


        #region Parametros
        //Obtener Parametros
        [Route("getParametrosById")] //Ruta de acceso al Metodo
        [HttpGet]  // Tipo de Respuesta
        [ResponseType(typeof(ParametrosByIdLista))]
        public IHttpActionResult GetListaParametrosById(string tipo, string token)
        {
            try
            {
                if (token.Equals(keyToken))
                {
                    var respuesta = srvParametricas.GetListaParametrosById(tipo);
                    return Ok(respuesta);
                }
                else
                    return null;
            }
            catch (Exception e)
            {
                string error = "Error desde el servicio GetListaParametrosById" + e.Message;
                return Ok(error);
            }
        }
        #endregion
        // GET: api/[controller]/getTree
        [Route("test")]
        [HttpGet]
        public string test()
        {
            return "Servidor responde";
        }



    }


}